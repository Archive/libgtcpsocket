/* 
 * LibGTcpSocket: libgtcpsocket/g-tcp-proxy-private.h
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_PROXY_PRIVATE_H__
#define __GTCP_PROXY_PRIVATE_H__ 1

#include "gtcp-socket-types.h"

G_BEGIN_DECLS
/* Proxy Data */
	typedef struct
{
	GTcpProxyType type:3;
	gboolean use_proxy:1;

	gchar *host;
	guint16 port;

	gchar *username;
	gchar *passwd;

	GMutex *mutex;

	gboolean use_auth:1;
}
GTcpProxy;


void _gtcp_proxy_initialize (void);
void _gtcp_proxy_shutdown (void);

G_CONST_RETURN GTcpProxy *_gtcp_proxy_get_proxy (GTcpProxyType type,
												 const gchar * address);
GTcpConnectionStatus _gtcp_proxy_traverse_proxy (GTcpProxy * proxy,
												 const gchar * address,
												 guint16 port,
												 gint sockfd,
												 gboolean is_server);


G_END_DECLS
#endif /* __G_TCP_PROXY_PRIVATE_H__ */
