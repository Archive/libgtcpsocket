
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>


int
main (int argc,
	  char *argv[])
{
	char varname[300],
	 typedef_name[300],
	 *define_name,
	 *enum_name,
	 *key,
	 *last,
	 line[300];
	FILE *in,
	 *out,
	 *key_tmp,
	 *enum_tmp,
	 *define_tmp;
	unsigned int i;

	if (argc < 3)
	{
		printf ("Error: You need to specifiy an input and output file:\n\t"
				"%s: [input] [output]\n", argv[0]);
		return (-1);
	}

	in = fopen (argv[1], "r");
	out = fopen (argv[2], "w");

	define_tmp = fopen (".defines.h", "w");
	enum_tmp = fopen (".enum.h", "w");
	key_tmp = fopen (".keys.h", "w");

	fgets (varname, 300, in);
	varname[strlen (varname) - 1] = '\0';
	fgets (typedef_name, 300, in);
	typedef_name[strlen (typedef_name) - 1] = '\0';
	printf ("Variable name: '%s'\nTypedef name: '%s'\n", varname, typedef_name);

	printf ("Writing temporary files.\n");

	fprintf (key_tmp, "static const char *const %s[] = {", varname);
	fputs ("typedef enum\n{", enum_tmp);

	if (fscanf (in, "%as %as %as", &define_name, &enum_name, &key) != EOF)
	{
		fprintf (define_tmp, "#define %s %s[%s]\n", define_name, varname, enum_name);
		fprintf (enum_tmp, "\n\t%s", enum_name);
		fprintf (key_tmp, "\n\t%s", key);
	}
	else
	{
		return (-1);
	}

	while (fscanf (in, "%as %as %as", &define_name, &enum_name, &key) != EOF)
	{
		fprintf (define_tmp, "#define %s %s[%s]\n", define_name, varname, enum_name);
		fprintf (enum_tmp, ",\n\t%s", enum_name);
		fprintf (key_tmp, ",\n\t%s", key);
	}

	fclose (define_tmp);

	last = strdup (typedef_name);
	for (i = 0; i < strlen (last); i++)
		last[i] = toupper (last[i]);

	fprintf (enum_tmp, ",\n\t%s_LAST\n}\n%s;\n\n", last, typedef_name);
	fclose (enum_tmp);

	fprintf (key_tmp, "\n};\n");
	fclose (key_tmp);


	printf ("Done writing temporary files.\n");

	printf ("Writing defines: ");
	define_tmp = fopen (".defines.h", "r");
	while (fgets (line, 300, define_tmp))
	{
		fputs (line, out);
	}
	fclose (define_tmp);
	fprintf (out, "\n\n");
	printf ("done.\n");

	printf ("Writing enum: ");
	enum_tmp = fopen (".enum.h", "r");
	while (fgets (line, 300, enum_tmp))
	{
		fputs (line, out);
	}
	fclose (enum_tmp);
	fprintf (out, "\n");
	printf ("done.\n");

	printf ("Writing key array: ");
	key_tmp = fopen (".keys.h", "r");
	while (fgets (line, 300, key_tmp))
	{
		fputs (line, out);
	}
	fclose (key_tmp);
	fprintf (out, "\n");
	printf ("done.\n");

	printf ("Deleting temporary files: ");
	unlink (".keys.h");
	unlink (".enum.h");
	unlink (".defines.h");
	printf ("done.\n");
	
	fclose (out);

	return (0);
}
