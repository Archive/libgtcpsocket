/*
 * LibGTcpSocket: libgtcpsocket/gtcp-i18n.h
 *
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * Copyright (C) 2002 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GTCP_I18N_H__
#define __GTCP_I18N_H__ 1

#include <glib.h>

G_BEGIN_DECLS


#ifdef ENABLE_NLS
#	include <libintl.h>
#	ifdef HAVE_GETTEXT
#		define _(str)		(gettext (str))
#		ifdef gettext_noop
#			define N_(str)	gettext_noop (str)
#		else
#			define N_(str)	(str)
#		endif /* gettext_noop */
#		ifdef dgettext
#			define D_(str)	(dgettext (GETTEXT_PACKAGE, str))
#		else
#			define D_(str)	(str)
#		endif /* dgettext */
#	else
#		define _(str)		(str)
#		define N_(str)		(str)
#		define D_(str)		(str)
#	endif /* HAVE_GETTEXT */
#else
/* Stubs that do something close enough.  */
#	define textdomain(str)					(str)
#	define gettext(str)						(str)
#	define dgettext(domain,str)				(str)
#	define dcgettext(domain,str,type)		(str)
#	define bindtextdomain(domain,directory) (domain)
#	define _(str)							(str)
#	define N_(str)							(str)
#	define D_(str)							(str)
#endif


G_END_DECLS

#endif /* __GTCP_I18N_H__ */
