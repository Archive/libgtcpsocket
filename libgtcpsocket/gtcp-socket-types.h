/* 
 * LibGTcpSocket: libgtcpsocket/gtcp-socket-types.h
 *
 * Copyright (C) 2002 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

Enumerated Data Types.

Notes on editing:
	Tab size: 4
*/

#ifndef __GTCP_SOCKET_TYPES_H__
#define __GTCP_SOCKET_TYPES_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * GTcpLookupStatus:
 * @GTCP_LOOKUP_IN_PROGRESS: currently in progress, should never be returned.
 * @GTCP_LOOKUP_OK: the lookup succeeded.
 * @GTCP_LOOKUP_ERROR_NOT_FOUND: there is no DNS record for the address.
 * @GTCP_LOOKUP_ERROR_NO_RECOVERY: the DNS server could not be found.
 * @GTCP_LOOKUP_ERROR_TRY_AGAIN: a temporary error occured. Try later.
 * @GTCP_LOOKUP_ERROR_IPV4_IPV6_MISMATCH: the network is misconfigured.
 * @GTCP_LOOKUP_ERROR_THREAD_ERROR: a problem with the GLib thread system occured.
 *
 * Used by gtcp_dns_get() callbacks and the "#GTcpConnection-lookup-done" signal.
 * User applications should use gtcp_error_get_lookup_status_message() to get
 * a properly formatted error string for use in an error message box.
 *
 * Since: 1.0
 **/
typedef enum					/*< prefix=GTCP_LOOKUP > */
{
	GTCP_LOOKUP_IN_PROGRESS = -1,

	GTCP_LOOKUP_OK = 0,

	GTCP_LOOKUP_ERROR_NOT_FOUND,
	GTCP_LOOKUP_ERROR_NO_RECOVERY,
	GTCP_LOOKUP_ERROR_TRY_AGAIN,
	GTCP_LOOKUP_ERROR_IPV4_IPV6_MISMATCH,
	GTCP_LOOKUP_ERROR_THREAD_ERROR
}
GTcpLookupStatus;


/**
 * GTcpConnectionStatus:
 * @GTCP_CONNECTION_ERROR_CONNECTION_REFUSED: the server refused your attempt.
 * @GTCP_CONNECTION_ERROR_TIMEOUT: the server never responded.
 * @GTCP_CONNECTION_ERROR_NETWORK_UNREACHABLE: the specified network is not available to you (typically the network connection is not up).
 * @GTCP_CONNECTION_ERROR_BAD_BROADCAST_OR_FIREWALL: you are attempting to use a broadcast address and do not have permission to do so, or a firewall rule is preventing the connection.
 * @GTCP_CONNECTION_ERROR_INTERNAL: an unrecoverable internal error occurred inside the GTcpSocket Library (usually indicative of a larger system problem).
 * @GTCP_CONNECTION_ERROR_THREAD_ERROR: a problem with the GLib thread system occured.
 * @GTCP_CONNECTION_ERROR_PROXY_ERROR: the proxy server could not be contacted.
 * @GTCP_CONNECTION_CLOSING: the connection is in the process of closing.
 * @GTCP_CONNECTION_CLOSED: the connection is closed.
 * @GTCP_CONNECTION_CONNECTED: the connection is open and ready.
 * @GTCP_CONNECTION_CONNECTING: the connection is trying to connect.
 *
 * Used by the "#GTcpConnection-connect-done" signal. User applications should
 * use gtcp_error_get_connection_status_message() to get a properly formatted
 * error string for use in an error message box.
 *
 * Since: 1.0.
 **/
typedef enum					/*< prefix=GTCP_CONNECTION > */
{
	GTCP_CONNECTION_ERROR_CONNECTION_REFUSED,
	GTCP_CONNECTION_ERROR_TIMEOUT,
	GTCP_CONNECTION_ERROR_NETWORK_UNREACHABLE,
	GTCP_CONNECTION_ERROR_BAD_BROADCAST_OR_FIREWALL,
	GTCP_CONNECTION_ERROR_INTERNAL,
	GTCP_CONNECTION_ERROR_THREAD_ERROR,
	GTCP_CONNECTION_ERROR_PROXY_ERROR,
	GTCP_CONNECTION_CLOSING,
	GTCP_CONNECTION_CLOSED,

	GTCP_CONNECTION_CONNECTED,
	GTCP_CONNECTION_CONNECTING
}
GTcpConnectionStatus;


/**
 * GTcpConnectionStyle:
 * @GTCP_CONNECTION_HTTP: connect through an HTTP proxy.
 * @GTCP_CONNECTION_FTP: connect through an FTP proxy (not yet implemented).
 * @GTCP_CONNECTION_OTHER: connect through a SOCKS server.
 *
 * The #GTcpConnection style, used in conjunction with the "#use-ssl"
 * property to determine what proxy to use. See also #GTcpProxyType.
 * 
 * Since: 1.0.
 **/
typedef enum					/*< prefix=GTCP_CONNECTION > */
{
	GTCP_CONNECTION_HTTP,
	GTCP_CONNECTION_FTP,
	GTCP_CONNECTION_OTHER
}
GTcpConnectionStyle;


/**
 * GTcpProxyType:
 * @GTCP_PROXY_HTTP: use an HTTP proxy.
 * @GTCP_PROXY_FTP: use an FTP proxy (not implemented).
 * @GTCP_PROXY_OTHER: use a SOCKS proxy of some type (this will be determined from the proxy configuration).
 * @GTCP_PROXY_SSL: use an SSL proxy (HTTPS should use HTTP proxies) (not implemented).
 * @GTCP_PROXY_SOCKS4: use a SOCKS version 4 server.
 * @GTCP_PROXY_SOCKS5: use a SOCKS version 5 server.
 *
 * The proxy type to use for a particular address. See also: #GTcpConnectionStyle.
 * 
 * Since: 1.0.
 **/
typedef enum					/* < prefix=GTCP_PROXY > */
{
	GTCP_PROXY_HTTP = GTCP_CONNECTION_HTTP,
	GTCP_PROXY_FTP = GTCP_CONNECTION_FTP,
	GTCP_PROXY_OTHER = GTCP_CONNECTION_OTHER,

	GTCP_PROXY_SSL,
	GTCP_PROXY_SOCKS4,
	GTCP_PROXY_SOCKS5
}
GTcpProxyType;


/**
 * GTcpSendStatus:
 * @GTCP_SEND_DATA_QUEUED: the data is queued for sending, and will be sent shortly.
 * @GTCP_SEND_ERROR: the data could not be sent.
 * @GTCP_SEND_ERROR_NOT_OPEN: the #GTcpConnection-struct is not open.
 * @GTCP_SEND_ERROR_NULL_DATA: there is no data to send.
 * 
 * Returned by gtcp_connection_send().
 * 
 * Since: 1.0
 **/
typedef enum					/*< prefix=GTCP_SEND > */
{
	GTCP_SEND_DATA_QUEUED,
	GTCP_SEND_ERROR,
	GTCP_SEND_ERROR_NOT_OPEN,
	GTCP_SEND_ERROR_NULL_DATA
}
GTcpSendStatus;

/**
 * GTcpServerKillStyle:
 * @GTCP_SERVER_LEAVE_OPEN: leave the connection objects opened by this server alone.
 * @GTCP_SERVER_KILL_ON_CLOSE: close the connection objects when the server is closed.
 * @GTCP_SERVER_KILL_ON_FINALIZE: close the connection objects when the server is finalized (with g_object_unref ()).
 * @GTCP_SERVER_FINALIZE_ON_CLOSE: unref the connection objects when the server is closed.
 * @GTCP_SERVER_FINALIZE_ON_FINALIZE: unref the connection objects when the server is finalized (with g_object_unref ()).
 *
 * The kill policy to use for #GTcpConnection objects that are opened by the server.
 *
 * Since: 1.0
 **/
typedef enum					/*< prefix=GTCP_SERVER > */
{
	GTCP_SERVER_LEAVE_OPEN,
	GTCP_SERVER_KILL_ON_CLOSE,
	GTCP_SERVER_KILL_ON_FINALIZE,
	GTCP_SERVER_FINALIZE_ON_CLOSE,
	GTCP_SERVER_FINALIZE_ON_FINALIZE
}
GTcpServerKillStyle;


/**
 * GTcpServerOpenStatus:
 * @GTCP_SERVER_OPEN_OK: the server is open and ready.
 * @GTCP_SERVER_OPEN_OPENING: the server is in the process of opening (gtcp_server_open() has not returned yet).
 * @GTCP_SERVER_OPEN_ERROR_NO_LOCAL_HOSTNAME: the address passed is not a local address, or there are no open network interfaces.
 * @GTCP_SERVER_OPEN_ERROR_SOCKET_TYPE_PERMS: the user does not have permission to open TCP/IP sockets (this is a system configuration error).
 * @GTCP_SERVER_OPEN_ERROR_NO_MEM: there is not enough available memory to open a new socket.
 * @GTCP_SERVER_OPEN_ERROR_TABLE_OVERFLOW: there are too many open sockets already.
 * @GTCP_SERVER_OPEN_ERROR_LOW_PORT_PERMS: the port is under 1024, and requires special permissions to use these ports.
 * @GTCP_SERVER_OPEN_ERROR_ALREADY_BOUND: there is already an open server (probably a service) on the requested port.
 * @GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR: an unrecoverable internal error occurred inside the GTcpSocket Library (usually indicative of a larger system problem).
 *
 * Used by gtcp_server_open() to provide a descriptive error on the status.
 * User applications should use gtcp_error_get_server_open_status_message() to get
 * a properly formatted error string for use in an error message box.
 *
 * Since: 1.0.
 **/
typedef enum					/* < prefix=GTCP_SERVER_OPEN > */
{
	GTCP_SERVER_OPEN_OK,
	GTCP_SERVER_OPEN_OPENING,

	GTCP_SERVER_OPEN_ERROR_NO_LOCAL_HOSTNAME,

	GTCP_SERVER_OPEN_ERROR_SOCKET_TYPE_PERMS,
	GTCP_SERVER_OPEN_ERROR_NO_MEM,
	GTCP_SERVER_OPEN_ERROR_TABLE_OVERFLOW,

	GTCP_SERVER_OPEN_ERROR_LOW_PORT_PERMS,
	GTCP_SERVER_OPEN_ERROR_ALREADY_BOUND,

	GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR
}
GTcpServerOpenStatus;


G_END_DECLS

#endif /* __GTCP_SOCKET_TYPES__ */
