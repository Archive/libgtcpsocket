/* LibGTcpSocket: src/gtcp-server.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#include "gtcp-server.h"
#include "gtcp-socket-types.h"
#include "gtcp-socket-type-builtins.h"
#include "gtcp-i18n.h"

#include "gtcp-dns.h"

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#ifndef GTCP_SOCKET_DEFAULT_BUFFER_SIZE
#	define GTCP_SOCKET_DEFAULT_BUFFER_SIZE 2048
#endif /* GTCP_SOCKET_DEFAULT_BUFFER_SIZE */


enum
{
	PROP_0,
	/* User-settable */
	PROP_LOCAL_ADDRESS,
	PROP_LOCAL_PORT,
	PROP_USE_SSL,
	PROP_BUFFER_SIZE,
	PROP_MAX_CONNECTIONS,
	PROP_CONNECTIONS,
	PROP_KILL_STYLE,
	PROP_DO_REVERSE_LOOKUPS,

	/* Considered read-only */
	PROP_BYTES_READ,
	PROP_BYTES_WRITTEN
};

enum
{
	INCOMING,
	CLOSED,
	LAST_SIGNAL
};


struct _GTcpServerPrivate
{
	/* Object Properties */
	gchar *address;
	guint port;
	gboolean use_ssl;

	gint max_connections;
	GList *connections;
	gboolean do_reverse_lookups;

	gsize buffer_size;

	gulong bytes_read;
	gulong bytes_written;

	/* Socket */
	gint sockfd;

	/* GSource */
	gint incoming_id;

	GTcpServerKillStyle kill_style:3;
	gboolean open:1;
};


static gpointer parent_class = NULL;
static gint signals[LAST_SIGNAL] = { 0 };


/* Utility Functions */
static void
conn_recv_cb (GTcpConnection * conn,
			  gconstpointer data,
			  gsize length,
			  gpointer server)
{
	GTCP_SERVER (server)->_priv->bytes_read += length;
	g_object_notify (G_OBJECT (server), "bytes-read");
}


static void
conn_send_cb (GTcpConnection * conn,
			  gconstpointer data,
			  gsize length,
			  gpointer server)
{
	GTCP_SERVER (server)->_priv->bytes_written += length;
	g_object_notify (G_OBJECT (server), "bytes-written");
}


static void
conn_closed_cb (GTcpConnection * conn,
				gpointer data)
{
	GTcpServer *server = GTCP_SERVER (data);

	server->_priv->connections = g_list_remove (server->_priv->connections, conn);
	g_object_notify (G_OBJECT (data), "connections");
}


static void
dns_callback (const GTcpDNSEntry * entry,
			  gpointer data)
{
	GObject *object = G_OBJECT (data);

	if (entry->hostname != NULL && entry->hostname[0] != '\0')
		g_object_set (object, "address", entry->hostname, NULL);

	g_signal_emit_by_name (object, "lookup-done", entry->status, NULL);
}


static gboolean
check_for_incoming (gpointer data)
{
	GTcpServer *server = GTCP_SERVER (data);
	GObject *conn = NULL;

	fd_set read_fds;
	struct timeval tv = { 0, 500 };

	if (!server->_priv->open)
		return FALSE;

	/* If there is a connection limit && we're already there */
	if (server->_priv->max_connections != -1 &&
		(gint) g_list_length (server->_priv->connections) >=
		server->_priv->max_connections)
		return TRUE;

	FD_ZERO (&read_fds);
	FD_SET (server->_priv->sockfd, &read_fds);

	select (server->_priv->sockfd + 1, &read_fds, NULL, NULL, &tv);

	if (FD_ISSET (server->_priv->sockfd, &read_fds))
	{
		struct sockaddr_in remote_sin,
		  local_sin;
		gint conn_fd;
		gchar *remote_address,
		 *local_address;
		gsize sin_size;

		conn_fd = accept (server->_priv->sockfd, (struct sockaddr *) &remote_sin,
						  &sin_size);
		/* For whatever reason the incoming connection failed */
		if (conn_fd < 0)
			return TRUE;

		remote_address = g_strdup (inet_ntoa (remote_sin.sin_addr));
		sin_size = sizeof (struct sockaddr_in);

		conn = (*GTCP_SERVER_GET_CLASS (server)->create_incoming) (server,
																   remote_address,
																   g_htons (remote_sin.
																			sin_port));

		if (conn != NULL)
		{
			/* Add it to the list of connections we've got */
			server->_priv->connections = g_list_prepend (server->_priv->connections, conn);

			/* Connect signals & tell the world we've got a new incoming connection */
			g_signal_connect (conn, "recv", (GCallback) conn_recv_cb, server);
			g_signal_connect (conn, "send", (GCallback) conn_send_cb, server);
			g_signal_connect (conn, "closed", (GCallback) conn_closed_cb, server);

			getsockname (conn_fd, (struct sockaddr *) &local_sin, &sin_size);
			local_address = g_strdup (inet_ntoa (local_sin.sin_addr));

			if (server->_priv->do_reverse_lookups)
				gtcp_dns_get (local_address, dns_callback, conn);

			g_signal_emit (data, signals[INCOMING], 0, conn);

			/*
			 * Set the socket-fd property for the GTcpConnection. This actually starts
			 * the thread that handles the connection, so we do it after we've connected
			 * the signals & told the world about it, so no data is lost.
			 */

			g_object_set (conn, "local-address", local_address,
						  "local-port", g_htons (local_sin.sin_port),
						  "use-ssl", server->_priv->use_ssl,
						  "buffer-size", server->_priv->buffer_size,
						  "is-server", TRUE, "socket-fd", conn_fd, NULL);

			g_free (local_address);
		}
		else
		{
			shutdown (conn_fd, SHUT_RDWR);
		}

		g_free (remote_address);
	}

	return TRUE;
}


static void
kill_all_connections (GTcpServer * server)
{
	GList *connections = server->_priv->connections;
	GTcpConnection *conn = NULL;

	while (server->_priv->connections != NULL)
	{
		conn = GTCP_CONNECTION (connections->data);

		if (conn != NULL)
			gtcp_connection_close (conn);

		conn = NULL;
	}
}


/* GTcpServer Callbacks */
static GObject *
gtcp_server_create_incoming (GTcpServer * server,
							 const gchar * remote_address,
							 guint port)
{
	return g_object_new (GTCP_TYPE_CONNECTION,
						 "address", remote_address,
						 "ip-address", remote_address, "port", port, NULL);
}


static void
gtcp_server_closed (GTcpServer * server)
{
	server->_priv->open = FALSE;

	if (server->_priv->sockfd >= 0)
	{
		shutdown (server->_priv->sockfd, SHUT_RDWR);
	}

	if (server->_priv->connections != NULL
		&& server->_priv->kill_style == GTCP_SERVER_KILL_ON_CLOSE)
	{
		kill_all_connections (server);
	}
}


/* GObject Callbacks */
static void
gtcp_server_get_property (GObject * object,
						  guint property,
						  GValue * value,
						  GParamSpec * param_spec)
{
	GTcpServer *server = GTCP_SERVER (object);

	switch (property)
	{
	case PROP_LOCAL_ADDRESS:
		g_value_set_string (value, server->_priv->address);
		break;
	case PROP_LOCAL_PORT:
		g_value_set_uint (value, server->_priv->port);
		break;
	case PROP_USE_SSL:
		g_value_set_boolean (value, server->_priv->use_ssl);
		break;

	case PROP_MAX_CONNECTIONS:
		g_value_set_int (value, server->_priv->max_connections);
		break;
	case PROP_CONNECTIONS:
		g_value_set_pointer (value, server->_priv->connections);
		break;
	case PROP_BUFFER_SIZE:
		g_value_set_uint (value, server->_priv->buffer_size);
		break;

	case PROP_DO_REVERSE_LOOKUPS:
		g_value_set_boolean (value, server->_priv->do_reverse_lookups);
		break;
	case PROP_KILL_STYLE:
		g_value_set_enum (value, server->_priv->kill_style);
		break;

	case PROP_BYTES_READ:
		g_value_set_ulong (value, server->_priv->bytes_read);
		break;
	case PROP_BYTES_WRITTEN:
		g_value_set_ulong (value, server->_priv->bytes_written);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}


static void
gtcp_server_set_property (GObject * object,
						  guint property,
						  const GValue * value,
						  GParamSpec * param_spec)
{
	GTcpServer *server = GTCP_SERVER (object);

	switch (property)
	{
	case PROP_LOCAL_ADDRESS:
		gtcp_server_set_local_address (server, g_value_get_string (value));
		break;
	case PROP_LOCAL_PORT:
		gtcp_server_set_local_port (server, g_value_get_uint (value));
		break;
	case PROP_USE_SSL:
		gtcp_server_set_use_ssl (server, g_value_get_boolean (value));
		break;

	case PROP_MAX_CONNECTIONS:
		gtcp_server_set_max_connections (server, g_value_get_int (value));
		break;
	case PROP_BUFFER_SIZE:
		gtcp_server_set_buffer_size (server, g_value_get_uint (value));
		break;

	case PROP_DO_REVERSE_LOOKUPS:
		gtcp_server_set_do_reverse_lookups (server, g_value_get_boolean (value));
		break;
	case PROP_KILL_STYLE:
		gtcp_server_set_kill_style (server, g_value_get_enum (value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}


static void
gtcp_server_finalize (GObject * object)
{
	GTcpServer *server = GTCP_SERVER (object);

	if (server->_priv->open)
		gtcp_server_close (server);

	if (server->_priv->kill_style == GTCP_SERVER_KILL_ON_FINALIZE)
		kill_all_connections (server);

	if (server->_priv->address)
		g_free (server->_priv->address);
}



/* GType Functions */
static void
gtcp_server_class_init (GTcpServerClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = gtcp_server_get_property;
	object_class->set_property = gtcp_server_set_property;
	object_class->finalize = gtcp_server_finalize;

	class->closed = gtcp_server_closed;
	class->create_incoming = gtcp_server_create_incoming;

	g_object_class_install_property (object_class, PROP_LOCAL_ADDRESS,
									 g_param_spec_string
									 ("local-address", _("Local Address"),
									  _("The hostname or IP address of this computer "
										"-- optional."), NULL,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_LOCAL_PORT,
									 g_param_spec_uint
									 ("port", "Local Port",
									  _("The port number to allow connections to."),
									  0, 65535, 0,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_USE_SSL,
									 g_param_spec_boolean
									 ("use-ssl", "Use SSL",
									  _("Whether or not to use SSL connections to this "
										"server."),
									  FALSE, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_MAX_CONNECTIONS,
									 g_param_spec_int
									 ("max-connections",
									  _("Maximum Simultaneous Connections"),
									  _("The maximum number of connections to allow."),
									  -1, G_MAXINT, -1,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_CONNECTIONS,
									 g_param_spec_pointer
									 ("connections", _("Open Connections"),
									  _("A GList of open connections."),
									  G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_KILL_STYLE,
									 g_param_spec_enum
									 ("kill-style", _("Kill Open Connections Style"),
									  _("Whether to kill open GTcpConnections or not "
										"and when."),
									  GTCP_TYPE_SERVER_KILL_STYLE,
									  GTCP_SERVER_LEAVE_OPEN,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_DO_REVERSE_LOOKUPS,
									 g_param_spec_boolean
									 ("do-reverse-lookups", _("Performs Reverse Lookups"),
									  _("Whether or not to perform reverse lookups on "
										"incoming connections."),
									  FALSE, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_BUFFER_SIZE,
									 g_param_spec_uint
									 ("buffer-size", _("Buffer Size"),
									  _("The max incoming buffer size."),
									  0, G_MAXUINT,
									  GTCP_SOCKET_DEFAULT_BUFFER_SIZE,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_BYTES_READ,
									 g_param_spec_ulong
									 ("bytes-read", _("Bytes Read"),
									  _("The total bytes read."),
									  0, G_MAXULONG, 0, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_BYTES_WRITTEN,
									 g_param_spec_ulong
									 ("bytes-written", _("Bytes Written"),
									  _("The total bytes written."),
									  0, G_MAXULONG, 0, G_PARAM_READABLE));


	signals[INCOMING] =
		g_signal_new ("incoming", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GTcpServerClass, incoming),
					  NULL, NULL, g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1,
					  GTCP_TYPE_CONNECTION);

	signals[CLOSED] =
		g_signal_new ("closed", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST, G_STRUCT_OFFSET (GTcpServerClass, closed),
					  NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}


static void
gtcp_server_instance_init (GTcpServer * server)
{
	server->_priv->address = NULL;
	server->_priv->connections = NULL;

	server->_priv->bytes_read = 0;
	server->_priv->bytes_written = 0;
}


GType
gtcp_server_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GTcpServerClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gtcp_server_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,				/* class data */
			sizeof (GTcpServer),
			0,					/* number of pre-allocs */
			(GInstanceInitFunc) gtcp_server_instance_init,
			NULL				/* value table */
		};

		object_type = g_type_register_static (G_TYPE_OBJECT, "GTcpServer", &type_info, 0);
	}

	return object_type;
}


/***********************************
 * PUBLIC API                      *
 ***********************************/


/**
 * gtcp_server_new:
 * @local_address: the local address to use (optional).
 * @use_ssl: whether to use SSL or not.
 * @local_port: the local port to allow connections through.
 *
 * Creates a new, unopened #GTcpServer-struct.
 *
 * Returns: a new #GTcpServer-struct.
 *
 * Since: 1.0
 **/
GObject *
gtcp_server_new (guint local_port,
				 gboolean use_ssl,
				 const gchar * local_address)
{
	return g_object_new (GTCP_TYPE_SERVER,
						 "local-address", local_address,
						 "local-port", local_port, "use-ssl", use_ssl, NULL);
}


/**
 * gtcp_server_open:
 * @server: the #GTcpServer to start accepting connections to.
 *
 * Causes the #GTcpServer to start accepting incoming connections.
 * 
 * Returns: a #GTcpServerOpenStatus for success or error
 *
 * Since: 1.0
 **/
GTcpServerOpenStatus
gtcp_server_open (GTcpServer * server)
{

	GTcpServerOpenStatus status = GTCP_SERVER_OPEN_OK;
	struct sockaddr_in sin;
	gint bind_retval,
	  listen_retval,
	  yes = 1;

	g_return_val_if_fail (server != NULL, GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR);
	g_return_val_if_fail (GTCP_IS_SERVER (server), GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR);
	g_return_val_if_fail (server->_priv->open == FALSE, GTCP_SERVER_OPEN_OPENING);
	g_return_val_if_fail (GTCP_SERVER_GET_CLASS (server)->create_incoming != NULL,
						  GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR);

	server->_priv->open = TRUE;
	server->_priv->bytes_read = 0;
	server->_priv->bytes_written = 0;

	/* Create the socket */
	errno = 0;
	server->_priv->sockfd = socket (AF_INET, SOCK_STREAM, 0);
	if (server->_priv->sockfd < 0)
	{
		switch (errno)
		{
		case EMFILE:
			status = GTCP_SERVER_OPEN_ERROR_TABLE_OVERFLOW;
			break;
		case EACCES:
			status = GTCP_SERVER_OPEN_ERROR_SOCKET_TYPE_PERMS;
			break;
		case ENFILE:
		case ENOBUFS:
		case ENOMEM:
			status = GTCP_SERVER_OPEN_ERROR_NO_MEM;
			break;
		default:
			status = GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR;
		}

		server->_priv->open = FALSE;
		return status;
	}

	setsockopt (server->_priv->sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof (int));
	fcntl (server->_priv->sockfd, F_SETFL, O_NONBLOCK);

	/*
	 * If we don't have a forced local address, or if the address given isn't an IP
	 * address, just use INADDR_ANY
	 */
	if (server->_priv->address == NULL
		|| inet_aton (server->_priv->address, &(sin.sin_addr)) == 0)
	{
		sin.sin_addr.s_addr = INADDR_ANY;
	}
	sin.sin_family = AF_INET;
	sin.sin_port = g_htons (server->_priv->port);

	/* Bind the socket */
	errno = 0;
	bind_retval = bind (server->_priv->sockfd, ((struct sockaddr *) (&sin)),
						sizeof (struct sockaddr_in));
	if (bind_retval < 0)
	{
		switch (errno)
		{
		case EINVAL:
			status = GTCP_SERVER_OPEN_ERROR_ALREADY_BOUND;
			break;
		case EACCES:
			status = GTCP_SERVER_OPEN_ERROR_LOW_PORT_PERMS;
			break;
		default:
			status = GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR;
			break;
		}

		shutdown (server->_priv->sockfd, SHUT_RDWR);
		server->_priv->open = FALSE;
		return status;
	}

	errno = 0;
	listen_retval = listen (server->_priv->sockfd, server->_priv->max_connections);
	if (listen_retval < 0)
	{
		switch (errno)
		{
		case EADDRINUSE:
			status = GTCP_SERVER_OPEN_ERROR_ALREADY_BOUND;
			break;
		default:
			status = GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR;
			break;
		}

		shutdown (server->_priv->sockfd, SHUT_RDWR);
		server->_priv->open = FALSE;
		return status;
	}

	server->_priv->incoming_id = g_timeout_add (500, check_for_incoming, server);

	return status;
}


/**
 * gtcp_server_close:
 * @server: the #GTcpServer to stop accepting connections to.
 *
 * Causes a #GTcpServer to stop accepting incoming connections.
 * If the #GTcpServer kill-style property is set to kill on close, any
 * GTcpConnections opened by this server will be closed.
 *
 * Since: 1.0
 **/
void
gtcp_server_close (GTcpServer * server)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));

	if (server->_priv->open == FALSE)
		return;

	g_signal_emit (G_OBJECT (server), signals[CLOSED], 0, NULL);
}


/**
 * gtcp_server_is_open:
 * @server: the #GTcpServer to check.
 *
 * Checks if a #GTcpServer is ready to accept incoming connections.
 * 
 * Returns: TRUE if it is ready, FALSE if it is not.
 *
 * Since: 1.0
 **/
gboolean
gtcp_server_is_open (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, FALSE);
	g_return_val_if_fail (GTCP_IS_SERVER (server), FALSE);

	return server->_priv->open;
}


/**
 * gtcp_server_get_local_address:
 * @server: the #GTcpServer to get the address to.
 *
 * Gets the local address a #GTcpServer is set to force, or %NULL if none is set.
 * The returned string should not be freed or modified.
 * 
 * Returns: the local address, or %NULL.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gtcp_server_get_local_address (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, NULL);
	g_return_val_if_fail (GTCP_IS_SERVER (server), NULL);

	return server->_priv->address;
}


/**
 * gtcp_server_set_local_address:
 * @server: the #GTcpServer to set the local address for.
 * @local_address: the new local IP address, or %NULL, to use any address.
 *
 * Sets the local IP address a #GTcpServer will attempt to force. If address is
 * NULL, the server will use the default local address (Recommended).
 *
 * Since: 1.0
 **/
void
gtcp_server_set_local_address (GTcpServer * server,
							   const gchar * address)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));
	g_return_if_fail (server->_priv->open == FALSE);

	if (server->_priv->address != NULL)
		g_free (server->_priv->address);

	server->_priv->address = g_strdup (address);
}


/**
 * gtcp_server_get_local_port:
 * @server: the #GTcpServer to get the port being used.
 *
 * Gets the local port a #GTcpServer is set to allow connections to.
 * 
 * Returns: a guint corresponding to the outgoing port.
 *
 * Since: 1.0
 **/
guint
gtcp_server_get_local_port (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, 0);
	g_return_val_if_fail (GTCP_IS_SERVER (server), 0);

	return server->_priv->port;
}


/**
 * gtcp_server_set_local_port:
 * @server: the #GTcpServer to set the outgoing port for.
 * @local_port: the new local port.
 *
 * Sets the local port a #GTcpServer will allow connections to.
 *
 * Since: 1.0
 **/
void
gtcp_server_set_local_port (GTcpServer * server,
							guint local_port)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));
	g_return_if_fail (server->_priv->open == FALSE);
	g_return_if_fail (local_port <= 65535);

	server->_priv->port = local_port;
}


/**
 * gtcp_server_get_use_ssl:
 * @server: the #GTcpServer to get whether SSL being used.
 *
 * Gets whether SSL will be used for connections to this #GTcpServer.
 * 
 * Returns: a gboolean corresponding to whether SSL will be used or not.
 *
 * Since: 1.0
 **/
gboolean
gtcp_server_get_use_ssl (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, FALSE);
	g_return_val_if_fail (GTCP_IS_SERVER (server), FALSE);

	return server->_priv->use_ssl;
}


/**
 * gtcp_server_set_use_ssl:
 * @server: the #GTcpServer to set the outgoing port for.
 * @use_ssl: whether to use SSL for incoming connections or not.
 *
 * Sets whether a #GTcpServer-struct will use SSL for new incoming connections.
 * 
 * Returns: a gboolean corresponding to whether SSL support was included or not.
 *
 * Since: 1.0
 **/
gboolean
gtcp_server_set_use_ssl (GTcpServer * server,
						 gboolean use_ssl)
{
	g_return_val_if_fail (server != NULL, FALSE);
	g_return_val_if_fail (GTCP_IS_SERVER (server), FALSE);

#ifdef USE_SSL
	server->_priv->use_ssl = use_ssl;
	return TRUE;
#else
	return FALSE;
#endif /* USE_SSL */
}


/**
 * gtcp_server_get_max_connections:
 * @server: the #GTcpServer to get the maximum allowed connections.
 *
 * Gets the maximum number of incoming connections a #GTcpServer will allow.
 * 
 * Returns: a gint corresponding to the maximum number of incoming connections.
 *
 * Since: 1.0
 **/
gint
gtcp_server_get_max_connections (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, 0);
	g_return_val_if_fail (GTCP_IS_SERVER (server), 0);

	return server->_priv->port;
}


/**
 * gtcp_server_set_max_connections:
 * @server: the #GTcpServer to set the maxumum allowed connections for.
 * @max_connections: the new maximum
 *
 * Sets the maximum number of incoming connections a #GTcpServer will allow.
 * Setting to zero will disallow any new connections, and %-1 will allow an
 * unlimited number of connections (Not recommended). Changing this property
 * for an open connection will not close any connections which are already
 * open, it will only affect whether or not to allow new connections.
 *
 * Since: 1.0
 **/
void
gtcp_server_set_max_connections (GTcpServer * server,
								 gint max_connections)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));

	server->_priv->max_connections = max_connections;
}


/**
 * gtcp_server_get_kill_style:
 * @server: the #GTcpServer to get the kill policy for.
 *
 * Gets the kill policy a #GTcpServer will follow for killing it's connections.
 * 
 * Returns: a #GTcpServerKillStyle enum corresponding to the kill policy.
 *
 * Since: 1.0
 **/
GTcpServerKillStyle
gtcp_server_get_kill_style (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, GTCP_SERVER_LEAVE_OPEN);
	g_return_val_if_fail (GTCP_IS_SERVER (server), GTCP_SERVER_LEAVE_OPEN);

	return server->_priv->kill_style;
}


/**
 * gtcp_server_set_kill_style:
 * @server: the #GTcpServer to set the kill policy for.
 * @kill_style: the #GTcpServerKillStyle to use.
 *
 * Sets the kill policy for a #GTcpServer. See #GTcpServerKillStyle for more
 * information.
 *
 * Since: 1.0
 **/
void
gtcp_server_set_kill_style (GTcpServer * server,
							GTcpServerKillStyle kill_style)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));
	g_return_if_fail (server->_priv->open == FALSE);

	server->_priv->kill_style = kill_style;
}


/**
 * gtcp_server_get_do_reverse_lookups:
 * @server: the #GTcpServer to get whether to perform reverse DNS lookups or not.
 *
 * Gets whether a reverse DNS lookup will be performed on incoming connections
 * to this #GTcpServer.
 * 
 * Returns: A gboolean corresponding to whether reverse DNS lookups will be
 * performed or not.
 *
 * Since: 1.0
 **/
gboolean
gtcp_server_get_do_reverse_lookups (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, FALSE);
	g_return_val_if_fail (GTCP_IS_SERVER (server), FALSE);

	return server->_priv->do_reverse_lookups;
}


/**
 * gtcp_server_set_do_reverse_lookups:
 * @server: the #GTcpServer to set the outgoing port for.
 * @do_reverse_lookups: whether to perform reverse DNS lookups or not.
 *
 * Sets whether a #GTcpServer will perform reverse DNS lookups on incoming
 * connections.
 *
 * Since: 1.0
 **/
void
gtcp_server_set_do_reverse_lookups (GTcpServer * server,
									gboolean do_reverse_lookups)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));

	server->_priv->do_reverse_lookups = do_reverse_lookups;
}


/**
 * gtcp_server_set_buffer_size:
 * @server: the #GTcpServer to change the default buffer size of.
 * @buffer_size: the new buffer size.
 * 
 * This sets the buffer size used for new #GTcpConnection objects.
 * See also: #GTCP_SOCKET_DEFAULT_BUFFER_SIZE.
 *
 * Since: 1.0
 **/
void
gtcp_server_set_buffer_size (GTcpServer * server,
							 gsize buffer_size)
{
	g_return_if_fail (server != NULL);
	g_return_if_fail (GTCP_IS_SERVER (server));

	server->_priv->buffer_size = buffer_size;
}


/**
 * gtcp_server_get_buffer_size:
 * @server: the #GTcpServer to retrieve the default buffer size of.
 *
 * This retrieves the buffer size used for new #GTcpConnection objects.
 * See also: #GTCP_SOCKET_DEFAULT_BUFFER_SIZE.
 * 
 * Returns: the current default buffer size.
 *
 * Since: 1.0
 **/
gsize
gtcp_server_get_buffer_size (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, 0);
	g_return_val_if_fail (GTCP_IS_SERVER (server), 0);

	return server->_priv->buffer_size;
}


/**
 * gtcp_server_get_bytes_read:
 * @server: the #GTcpServer to get the number of bytes read from.
 *
 * This gets the number of bytes recieved by a #GTcpServer for all
 * connections. This counter is reset to zero when the server is opened,
 * so if you close the #GTcpServer, then open it, the counter will be
 * reset.
 * 
 * Returns: the number of bytes read so far.
 *
 * Since: 1.0
 **/
gulong
gtcp_server_get_bytes_read (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, 0);
	g_return_val_if_fail (GTCP_IS_SERVER (server), 0);

	return server->_priv->bytes_read;
}


/**
 * gtcp_server_get_bytes_written:
 * @server: the #GTcpServer to get the number of bytes written to.
 *
 * This gets the number of bytes recieved by a #GTcpServer for all
 * connections. This counter is reset to zero when the server is opened,
 * so if you close the #GTcpServer, then open it, the counter will be
 * reset.
 * 
 * Returns: the number of bytes sent so far.
 *
 * Since: 1.0
 **/
gulong
gtcp_server_get_bytes_written (GTcpServer * server)
{
	g_return_val_if_fail (server != NULL, 0);
	g_return_val_if_fail (GTCP_IS_SERVER (server), 0);

	return server->_priv->bytes_written;
}
