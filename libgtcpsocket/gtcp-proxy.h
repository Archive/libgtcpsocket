/* 
 * LibGTcpSocket: libgtcpsocket/gtcp-proxy.h
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_PROXY_H__
#define __GTCP_PROXY_H__ 1

#include "gtcp-socket-types.h"

G_BEGIN_DECLS


gboolean gtcp_proxy_get_use_proxy (GTcpProxyType type,
								   const gchar *address);


G_END_DECLS

#endif /* __GTCP_PROXY_H__ */
