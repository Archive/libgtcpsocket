/* 
 * LibGTcpSocket: libgtcpsocket/gtcp-errors.h
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

Get GErrors/error strings for various status messages.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_ERRORS_H__
#define __GTCP_ERRORS_H__ 1

#include <glib.h>
#include "gtcp-socket-types.h"

G_BEGIN_DECLS


gchar *gtcp_error_get_lookup_status_message (GTcpLookupStatus status,
											 const gchar * address);
gchar *gtcp_error_get_connection_status_message (GTcpConnectionStatus status,
												 const gchar * address,
												 guint port);
gchar *gtcp_error_get_server_open_status_message (GTcpServerOpenStatus status,
												  guint port);


G_END_DECLS

#endif /* __GTCP_ERRORS_H__ */
