/* 
 * LibGTcpSocket: libgtcpsocket/testgtcpconnection.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gtcp-connection.h"
#include "gtcp-errors.h"
#include "gtcp-i18n.h"

#include <stdio.h>


#ifdef G_LOG_DOMAIN
#	undef G_LOG_DOMAIN
#endif /* G_LOG_DOMAIN */
#define G_LOG_DOMAIN "TestGTcpConnection"


#define TESTGTCPCONNECTION_ADDR "irc.us.gimp.org"
#define TESTGTCPCONNECTION_PORT 6667
#define BOOLEAN_TO_STRING(val) (val ? "TRUE" : "FALSE")

GMainLoop *loop = NULL;


gboolean
quit_me (gpointer conn)
{
	g_message ("Quitting testgtcpconnection.");
	g_main_loop_quit (loop);
	return FALSE;
}

void
close_cb (GTcpConnection * conn,
		  gboolean requested,
		  gpointer blah)
{
	g_message ("Connection closed, unref'ing object.");
	g_object_unref (conn);

	g_message ("Requested = %s", BOOLEAN_TO_STRING (requested));

	g_message ("Socket closed. Quitting in 5 seconds...");
	g_timeout_add (5000, quit_me, conn);
}

gboolean
do_close (gpointer data)
{
	g_message ("Closing connection...");
	gtcp_connection_close (GTCP_CONNECTION (data));

	return FALSE;
}

void
do_send_test (GTcpConnection * conn)
{
//  gchar *test = "GET /index.html HTTP/1.0\r\n\r\n";
	gchar *test = "PASS mypassword\r\n"
				  "NICK TestGTcpConnection\r\n"
				  "PASS mypassword\r\n"
				  "USER test * irc.us.gimp.org :Testing TestGTcpConnection\r\n";

	if (gtcp_connection_send (conn, test, -1) == GTCP_SEND_DATA_QUEUED)
	{
		g_message ("Send test reported success.");
		g_timeout_add (30000, do_close, conn);
	}
	else
	{
		g_message ("gtcp_connection_send failed.\n");
		gtcp_connection_close (conn);
	}
}

void
lookup_done_cb (GTcpConnection * conn,
				GTcpLookupStatus status,
				gpointer user_data)
{
	g_message ("Connecting...");
	if (status != GTCP_LOOKUP_OK)
	{
		g_message ("Lookup failed: %s",
				   gtcp_error_get_lookup_status_message (status,
														 TESTGTCPCONNECTION_ADDR));
		g_message ("Exiting...");
		g_main_loop_quit (loop);
	}
}

void
connect_done_cb (GTcpConnection * conn,
				 GTcpConnectionStatus status)
{
	if (status != GTCP_CONNECTION_CONNECTED)
	{
		g_message ("Error connecting: %s",
				   gtcp_error_get_connection_status_message (status,
															 TESTGTCPCONNECTION_ADDR,
															 6667));
		g_message ("Exiting testgtcpconnection.\n");
		g_main_loop_quit (loop);
		return;
	}

	g_message ("Success Connecting\n");
	do_send_test (conn);
}


void
recv_cb (GTcpConnection * conn,
		 gconstpointer data,
		 gsize length,
		 gpointer user_data)
{
	g_message ("recv:");
	printf ("data:\n'%s'\nlength: '%u'\n\n", (gchar *) data, length);
	g_message ("Total bytes received: '%lu'\n", gtcp_connection_get_bytes_read (conn));
}

void
send_cb (GTcpConnection * conn,
		 gconstpointer data,
		 gsize length,
		 gpointer user_data)
{
	if (length <= 0)
	{
		g_message ("I'm guessing we've got some bad data, length == %d, but data = %s",
				   length, (const gchar *) data);
		g_main_loop_quit (loop);
	}

	g_message ("Send:");
	printf ("Data:\n'%s'\n\nLength: '%u'\n\n", (gchar *) data, length);
	g_print ("Total bytes sent: '%lu'\n", gtcp_connection_get_bytes_written (conn));
}


gboolean
do_testgtcpconnection (gpointer data)
{
	GObject *conn = NULL;

	g_message ("Creating object.");

	conn = gtcp_connection_new (TESTGTCPCONNECTION_ADDR, TESTGTCPCONNECTION_PORT,
								GTCP_CONNECTION_HTTP, FALSE);

	g_signal_connect (G_OBJECT (conn), "lookup-done", G_CALLBACK (lookup_done_cb), NULL);
	g_signal_connect (G_OBJECT (conn), "connect-done",
					  G_CALLBACK (connect_done_cb), NULL);
	g_signal_connect (G_OBJECT (conn), "recv", G_CALLBACK (recv_cb), NULL);
	g_signal_connect (G_OBJECT (conn), "send", G_CALLBACK (send_cb), NULL);
	g_signal_connect (G_OBJECT (conn), "closed", G_CALLBACK (close_cb), NULL);

	g_message ("Opening connection asynchronously.");
	gtcp_connection_open (GTCP_CONNECTION (conn));

	g_message ("Open function has returned, looking up %s:%d...\n",
			   TESTGTCPCONNECTION_ADDR, TESTGTCPCONNECTION_PORT);

	return FALSE;
}


int
main (int argc,
	  char *argv[])
{
	g_type_init_with_debug_flags (G_TYPE_DEBUG_OBJECTS | G_TYPE_DEBUG_SIGNALS);
	loop = g_main_loop_new (NULL, FALSE);

	g_idle_add (do_testgtcpconnection, NULL);

	g_main_loop_run (loop);

	return 0;
}
