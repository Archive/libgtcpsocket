/* LibGTcpSocket: src/gtcp-dns.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0 DNS lookup backend.

Notes on editing:
	Tab size: 4
*/


#include <glib.h>

#include "gtcp-dns.h"

#include <time.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>


#define GTCP_DNS_CLEANUP_INTERVAL 2400000


#define set_handle(lookup,retval) \
G_STMT_START{ \
	retval = lookup->handle = dns->current_handle; \
	dns->current_handle++; \
} G_STMT_END


typedef struct
{
	GSList *cache;
	GStaticMutex *cache_mutex;

	gint cache_cleanup_id;
	GTcpDNSHandle current_handle;

	guint ref;
}
GTcpDNS;


typedef struct
{
	GSList *hashes;

	gchar *lookup_addr;

	GThread *lookup_thread;
	GStaticMutex *in_progress_mutex;

	GSList *lookups;
	GStaticMutex *lookups_mutex;

	guint thread_done_id;
	GTime mod_time;

	GTcpDNSEntry *dns_entry;	/* Glib-esque hostent */
}
GTcpDNSCacheEntry;


typedef struct
{
	GTcpDNSHandle handle;
	GTcpDNSCallbackFunc callback;
	gpointer user_data;
	GDestroyNotify destroy_data;
}
GTcpDNSWaitingLookup;


static GTcpDNS *dns = NULL;


static GTcpDNSEntry *
gtcp_dns_entry_new (void)
{
	GTcpDNSEntry *entry = NULL;

	entry = g_new0 (GTcpDNSEntry, 1);
	g_assert (entry != NULL);

	entry->status = GTCP_LOOKUP_IN_PROGRESS;
	entry->hostname = NULL;
	entry->aliases = NULL;
	entry->ip_addresses = NULL;

	return entry;
}


static GTcpDNSCacheEntry *
gtcp_dns_cache_entry_new (void)
{
	GTcpDNSCacheEntry *entry = NULL;

	entry = g_new0 (GTcpDNSCacheEntry, 1);
	g_assert (entry != NULL);

	entry->hashes = NULL;
	entry->lookup_thread = NULL;
	entry->lookups = NULL;

	entry->lookups_mutex = NULL;
	entry->lookups_mutex = g_new0 (GStaticMutex, 1);
	g_assert (entry->lookups_mutex != NULL);
	g_static_mutex_init (entry->lookups_mutex);

	entry->in_progress_mutex = NULL;
	entry->in_progress_mutex = g_new0 (GStaticMutex, 1);
	g_assert (entry->in_progress_mutex != NULL);
	g_static_mutex_init (entry->in_progress_mutex);

	entry->dns_entry = gtcp_dns_entry_new ();

	entry->mod_time = time (NULL);

	return entry;
}


static void
gtcp_dns_entry_real_free (gpointer data)
{
	GTcpDNSEntry *entry = NULL;

	g_return_if_fail (data != NULL);

	entry = GTCP_DNS_ENTRY (data);

	if (entry->hostname != NULL)
		g_free (entry->hostname);

	while (entry->aliases != NULL)
	{
		if (entry->aliases->data != NULL)
			g_free (entry->aliases->data);

		entry->aliases = g_slist_remove (entry->aliases, entry->aliases->data);
	}

	while (entry->ip_addresses != NULL)
	{
		if (entry->ip_addresses->data != NULL)
			g_free (entry->ip_addresses->data);

		entry->ip_addresses = g_slist_remove (entry->ip_addresses,
											  entry->ip_addresses->data);
	}
}


static void
gtcp_dns_cache_entry_free (GTcpDNSCacheEntry * entry)
{
	GTcpDNSWaitingLookup *lookup;

	g_return_if_fail (entry != NULL);

	g_static_mutex_lock (dns->cache_mutex);
	dns->cache = g_slist_remove (dns->cache, entry);
	g_static_mutex_unlock (dns->cache_mutex);

	g_slist_free (entry->hashes);

	while (entry->lookups != NULL)
	{
		lookup = (GTcpDNSWaitingLookup *) entry->lookups->data;

		lookup->callback = NULL;
		if (lookup->destroy_data != NULL && lookup->user_data != NULL)
			(*lookup->destroy_data) (lookup->user_data);
		lookup->user_data = NULL;

		g_free (lookup);

		entry->lookups = g_slist_remove (entry->lookups, lookup);
	}

	g_static_mutex_free (entry->lookups_mutex);
	g_static_mutex_free (entry->in_progress_mutex);

	gtcp_dns_entry_free (entry->dns_entry);

	g_free (entry);
}


static void
gtcp_dns_shutdown ()
{
	GTcpDNS *tmp_dns = dns;

	if (dns == NULL)
		return;

	dns->ref--;

	if (dns->ref > 0)
		return;

	g_source_remove (tmp_dns->cache_cleanup_id);
	dns = NULL;

	while (tmp_dns->cache != NULL)
	{
		gtcp_dns_cache_entry_free (tmp_dns->cache->data);
		tmp_dns->cache = g_slist_remove (tmp_dns->cache, tmp_dns->cache->data);
	}

	g_static_mutex_free (tmp_dns->cache_mutex);

	g_free (tmp_dns);
}


static gboolean
cache_cleanup (gpointer user_data)
{
	GTcpDNSCacheEntry *entry;
	GSList *list;
	GTime current_time;

	current_time = time (NULL);

	for (list = dns->cache; list != NULL; list = list->next)
	{
		entry = (GTcpDNSCacheEntry *) list->data;

		if ((current_time - entry->mod_time) > GTCP_DNS_CLEANUP_INTERVAL)
		{
			if (g_slist_length (dns->cache) == 1)
				gtcp_dns_shutdown ();
			else
				gtcp_dns_cache_entry_free (entry);
		}
	}

	return TRUE;
}


static void
gtcp_dns_init ()
{
	if (dns != NULL)
	{
		dns->ref++;
		return;
	}

	dns = g_new0 (GTcpDNS, 1);
	g_assert (dns != NULL);

	dns->cache = NULL;

	dns->cache_mutex = NULL;
	dns->cache_mutex = g_new0 (GStaticMutex, 1);
	g_assert (dns->cache_mutex != NULL);
	g_static_mutex_init (dns->cache_mutex);

	dns->current_handle = 0;

	dns->ref = 1;
	dns->cache_cleanup_id = g_timeout_add (GTCP_DNS_CLEANUP_INTERVAL,
										   cache_cleanup, NULL);

#ifdef G_THREADS_ENABLED
	if (!g_thread_supported ())
		g_thread_init (NULL);
#endif /* G_THREADS_ENABLED */
}


static void
parse_hostent (GTcpDNSCacheEntry * entry,
			   struct hostent *host,
			   gint host_error_num) 
{
	guint i;
	gchar *dotted_quad;

	if (!host)
	{
		switch (host_error_num)
		{
		case HOST_NOT_FOUND:
			entry->dns_entry->status = GTCP_LOOKUP_ERROR_NOT_FOUND;
			break;

		case TRY_AGAIN:
			entry->dns_entry->status = GTCP_LOOKUP_ERROR_TRY_AGAIN;
			break;

		case NO_ADDRESS:
			entry->dns_entry->status = GTCP_LOOKUP_ERROR_IPV4_IPV6_MISMATCH;
			break;

			/* We make NO_RECOVERY the default, since something evil has
			   happened if it gets to default: by it's own, and NO_RECOVERY is
			   pretty evil, and indicates something is seriously wrong. */
		default:
			entry->dns_entry->status = GTCP_LOOKUP_ERROR_NO_RECOVERY;
			break;
		}
	}
	/* host != NULL */
	else
	{
		entry->dns_entry->status = GTCP_LOOKUP_OK;

		/* Cannonical name */
		entry->dns_entry->hostname = g_strdup (host->h_name);

		entry->hashes = g_slist_append (entry->hashes,
										GUINT_TO_POINTER (g_str_hash (host->h_name)));

		for (i = 0; host->h_aliases != NULL && host->h_aliases[i] != NULL; i++)
		{
			entry->dns_entry->aliases =
				g_slist_append (entry->dns_entry->aliases, g_strdup (host->h_aliases[i]));

			entry->hashes =
				g_slist_append (entry->hashes,
								GUINT_TO_POINTER (g_str_hash (host->h_aliases[i])));
		}

		for (i = 0; host->h_addr_list[i] != NULL; i++)
		{
			dotted_quad =
				g_strdup_printf ("%u.%u.%u.%u",
								 (guint8) host->h_addr_list[i][0],
								 (guint8) host->h_addr_list[i][1],
								 (guint8) host->h_addr_list[i][2],
								 (guint8) host->h_addr_list[i][3]);
			entry->dns_entry->ip_addresses =
				g_slist_append (entry->dns_entry->ip_addresses, dotted_quad);

			entry->hashes =
				g_slist_append (entry->hashes,
								GUINT_TO_POINTER (g_str_hash (dotted_quad)));
		}
	}

}


static gpointer
fwd_lookup_thread (GTcpDNSCacheEntry * entry)
{
	struct hostent *host = NULL;

	g_assert (entry != NULL);

	g_static_mutex_lock (entry->in_progress_mutex);
	host = gethostbyname (entry->lookup_addr);

	parse_hostent (entry, host, h_errno);

	g_static_mutex_unlock (entry->in_progress_mutex);
	g_thread_exit (NULL);
	return NULL;
}


static gpointer
rev_lookup_thread (GTcpDNSCacheEntry * entry)
{
	struct hostent *host = NULL;

	g_assert (entry != NULL);

	g_static_mutex_lock (entry->in_progress_mutex);
	host = gethostbyaddr (entry->lookup_addr, strlen (entry->lookup_addr), AF_INET);

	parse_hostent (entry, host, h_errno);

	g_static_mutex_unlock (entry->in_progress_mutex);
	g_thread_exit (NULL);
	return NULL;
}


static gboolean
run_callbacks (GTcpDNSCacheEntry * entry)
{
	GTcpDNSWaitingLookup *lookup;

	g_static_mutex_lock (entry->lookups_mutex);

	while (entry->lookups != NULL)
	{
		lookup = (GTcpDNSWaitingLookup *) entry->lookups->data;
		lookup->callback (entry->dns_entry, lookup->user_data);

		if (lookup->destroy_data != NULL && lookup->user_data != NULL)
			(*lookup->destroy_data) (lookup->user_data);

		g_free (lookup);
		entry->lookups = g_slist_remove (entry->lookups, lookup);
	}

	g_static_mutex_unlock (entry->lookups_mutex);

	return FALSE;
}


static gboolean
thread_joiner (gpointer data)
{
	GTcpDNSCacheEntry *entry = (GTcpDNSCacheEntry *) data;

	if (g_static_mutex_trylock (entry->in_progress_mutex))
	{
		g_timeout_add (25, (GSourceFunc) run_callbacks, data);
		g_static_mutex_unlock (entry->in_progress_mutex);
		return FALSE;
	}

	return TRUE;
}


/**
 * gtcp_dns_get_full:
 * @address: the local address to get the #GTcpDNSEntry for.
 * @callback: the callback to be called when the lookup has completed.
 * @user_data: the user data to pass to the callback.
 * @destroy_data: a function capable of freeing @user_data, or %NULL.
 *
 * This function performs an asynchronous DNS lookup (or reverse lookup)
 * on the hostname or IP address in @address. Note that @callback may be
 * called before this function returns if the DNS record for @address is
 * in the cache. After @callback has been called, @user_data will be
 * destroyed using @destroy_data.
 *
 * Returns: a #GTcpDNSHandle used to cancel the callback.
 *
 * Since: 1.0
 **/
GTcpDNSHandle
gtcp_dns_get_full (const gchar * address,
				   GTcpDNSCallbackFunc callback,
				   gpointer user_data,
				   GDestroyNotify destroy_data)
{
	GTcpDNSCacheEntry *entry;
	GTcpDNSWaitingLookup *lookup;
	GSList *cache_list,
	 *hash_list;
	guint addr_hash;
	struct in_addr addr;
	GTcpDNSHandle retval = GTCP_DNS_INVALID_HANDLE;

	g_return_val_if_fail (address != NULL, GTCP_DNS_INVALID_HANDLE);
	g_return_val_if_fail (address[0] != 0, GTCP_DNS_INVALID_HANDLE);
	g_return_val_if_fail (strlen (address) < 1024, GTCP_DNS_INVALID_HANDLE);
	g_return_val_if_fail (user_data != NULL || (user_data == NULL && destroy_data == NULL),
						  GTCP_DNS_INVALID_HANDLE);
	g_return_val_if_fail (callback != NULL, GTCP_DNS_INVALID_HANDLE);

	gtcp_dns_init ();

	addr_hash = g_str_hash (address);

	for (cache_list = dns->cache; cache_list != NULL; cache_list = cache_list->next)
	{
		entry = (GTcpDNSCacheEntry *) (cache_list->data);

		for (hash_list = entry->hashes; hash_list != NULL; hash_list = hash_list->next)
		{
			if (addr_hash == GPOINTER_TO_UINT (hash_list->data))
			{
				if (entry->dns_entry->status >= GTCP_LOOKUP_OK)
				{
					(*callback) (entry->dns_entry, user_data);

					if (destroy_data != NULL && user_data != NULL)
						(*destroy_data) (user_data);
				}
				else
				{
					lookup = NULL;
					lookup = g_new0 (GTcpDNSWaitingLookup, 1);
					g_assert (lookup != NULL);

					set_handle (lookup, retval);
					lookup->callback = callback;
					lookup->user_data = user_data;
					lookup->destroy_data = destroy_data;

					g_static_mutex_lock (entry->lookups_mutex);
					entry->lookups = g_slist_append (entry->lookups, lookup);
					g_static_mutex_unlock (entry->lookups_mutex);

					entry->mod_time = time (NULL);
				}

				return retval;
			}
		}
	}

	entry = gtcp_dns_cache_entry_new ();
	entry->lookup_addr = g_strdup (address);

	lookup = NULL;
	lookup = g_new0 (GTcpDNSWaitingLookup, 1);
	g_assert (lookup != NULL);

	set_handle (lookup, retval);
	lookup->callback = callback;
	lookup->user_data = user_data;
	entry->lookups = g_slist_append (entry->lookups, lookup);

	g_static_mutex_lock (dns->cache_mutex);
	dns->cache = g_slist_append (dns->cache, entry);
	g_static_mutex_unlock (dns->cache_mutex);

	if (inet_aton (address, &addr))
	{
		entry->dns_entry->hostname = g_strdup (address);
		entry->lookup_thread = g_thread_create ((GThreadFunc) rev_lookup_thread,
												entry, FALSE, NULL);
	}
	else
	{
		entry->lookup_thread = g_thread_create ((GThreadFunc) fwd_lookup_thread,
												entry, FALSE, NULL);
	}

	if (entry->lookup_thread == NULL)
	{
		entry->dns_entry->status = GTCP_LOOKUP_ERROR_THREAD_ERROR;
		run_callbacks (entry);
	}
	else
	{
		entry->thread_done_id = g_timeout_add (500, thread_joiner, entry);
	}

	return retval;
}


/**
 * gtcp_dns_cancel:
 * @handle: a #GTcpDNSHandle for a running lookup.
 *
 * This function prevents an asynchronous DNS lookup (or reverse lookup)
 * from calling it's callback unless it has already been called. NOTE: This
 * function will not actually stop a DNS lookup, only prevent the callback
 * associated with @handle from being called. (The lookup will still finish
 * and the results will still be cached.)
 *
 * Since: 1.0
 **/
void
gtcp_dns_cancel (GTcpDNSHandle handle)
{
	GSList *list,
	 *lookups_list;
	GTcpDNSCacheEntry *entry;
	GTcpDNSWaitingLookup *lookup;

	g_static_mutex_lock (dns->cache_mutex);
	for (list = dns->cache; list != NULL; list = list->next)
	{
		entry = (GTcpDNSCacheEntry *) list->data;

		g_static_mutex_lock (entry->lookups_mutex);
		for (lookups_list = entry->lookups;
			 lookups_list != NULL; lookups_list = lookups_list->next)
		{
			lookup = (GTcpDNSWaitingLookup *) entry->lookups->data;

			if (lookup->handle == handle)
			{
				entry->lookups = g_slist_remove (entry->lookups, lookup);

				if (lookup->destroy_data != NULL && lookup->user_data != NULL)
					(*lookup->destroy_data) (lookup->user_data);

				g_free (lookup);
			}
		}
		g_static_mutex_unlock (entry->lookups_mutex);
	}
	g_static_mutex_unlock (dns->cache_mutex);
}


/**
 * gtcp_dns_entry_free:
 * @entry: the #GTcpDNSEntry to free.
 *
 * This function frees a #GTcpDNSEntry.
 *
 * Since: 1.0
 **/
void
gtcp_dns_entry_free (GTcpDNSEntry * entry)
{
	gtcp_dns_entry_real_free (entry);
}


/**
 * gtcp_dns_entry_copy:
 * @src: the entry to copy.
 *
 * Copies an existing a #GTcpDNSEntry. The returned data should be freed
 * with gtcp_dns_entry_free() when no longer needed.
 *
 * Returns: a copy of @src.
 *
 * Since: 1.0
 **/
GTcpDNSEntry *
gtcp_dns_entry_copy (const GTcpDNSEntry * src)
{
	GSList *list;
	GTcpDNSEntry *dest;

	g_return_val_if_fail (src != NULL, NULL);

	dest = gtcp_dns_entry_new ();

	dest->status = src->status;
	dest->hostname = g_strdup (src->hostname);
	dest->aliases = NULL;
	dest->ip_addresses = NULL;

	for (list = src->aliases; list != NULL; list = list->next)
		dest->aliases = g_slist_append (dest->aliases, g_strdup (list->data));

	for (list = src->ip_addresses; list != NULL; list = list->next)
		dest->ip_addresses = g_slist_append (dest->ip_addresses, g_strdup (list->data));

	return dest;
}


GType
gtcp_dns_entry_get_type (void)
{
	static GType type = 0;

	if (type == 0)
	{
		type = g_boxed_type_register_static ("GTcpDNSEntry",
											 (GBoxedCopyFunc) gtcp_dns_entry_copy,
											 gtcp_dns_entry_real_free);
	}

	return type;
}
