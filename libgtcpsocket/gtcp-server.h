/* LibGTcpSocket: src/g-tcp-server.h
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_SERVER_H__
#define __GTCP_SERVER_H__


#include "gtcp-connection.h"


#define GTCP_TYPE_SERVER			(gtcp_server_get_type ())
#define GTCP_SERVER(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTCP_TYPE_SERVER, GTcpServer))
#define GTCP_SERVER_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GTCP_TYPE_SERVER, GTcpServerClass))
#define GTCP_IS_SERVER(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTCP_TYPE_SERVER))
#define GTCP_IS_SERVER_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GTCP_TYPE_SERVER))
#define GTCP_SERVER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GTCP_TYPE_SERVER, GTcpServerClass))


G_BEGIN_DECLS

typedef struct _GTcpServer GTcpServer;
typedef struct _GTcpServerClass GTcpServerClass;
typedef struct _GTcpServerPrivate GTcpServerPrivate;


/**
 * GTcpServerCreateIncomingFunc:
 * @server: the server receiving the request for a new connection.
 * @remote_address: the IP address of the client requesting a new connection.
 * @port: the remote port of the client requesting a new connection.
 * 
 * This user-function is called when a client requests a new connection to this
 * #GTcpServer. It should return a new #GTcpConnection object (with the "address"
 * and "port" properties set). #GTcpServer will set additional properties. To
 * close the incoming connection, return NULL.
 *
 * Returns: a new #GTcpConnection object.
 **/
typedef GObject *(*GTcpServerCreateIncomingFunc) (GTcpServer * server,
												  const gchar * remote_address,
												  guint port);


struct _GTcpServer
{
	/*<private> */
	GObject parent;

	GTcpServerPrivate *_priv;
};

/**
 * GTcpServerClass:
 * @incoming: the incoming connection signal's object callback.
 * @closed: the closed signal's object callback.
 * @create_incoming: the classwide virtual table callback to create a new connection object.
 *
 * The class structure for #GTcpServer objects. The @create_incoming member should
 * be initialized by a subclass' class_init function to point to a subclass-specific
 * function which creates a new GTcpConnection object or subclass when called.
 **/
struct _GTcpServerClass
{
	/* < private > */
	GObjectClass parent_class;

	/* < public > */
	/* Signals */
	void (*incoming)	(GTcpServer *server,
						 GTcpConnection *conn);
	void (*closed)		(GTcpServer *server);

	/* VTable */
	GTcpServerCreateIncomingFunc create_incoming;

	/* < private > */
	void (*__gtcp_reserved_1);
	void (*__gtcp_reserved_2);
	void (*__gtcp_reserved_3);
	void (*__gtcp_reserved_4);
};


GType gtcp_server_get_type (void);


GObject *gtcp_server_new (guint local_port,
						  gboolean use_ssl,
						  const gchar * local_address);

GTcpServerOpenStatus gtcp_server_open (GTcpServer * server);
void gtcp_server_close (GTcpServer * server);

gboolean gtcp_server_is_open (GTcpServer * server);

G_CONST_RETURN gchar *gtcp_server_get_local_address (GTcpServer * server);
void gtcp_server_set_local_address (GTcpServer * server,
									const gchar * local_address);

guint gtcp_server_get_local_port (GTcpServer * server);
void gtcp_server_set_local_port (GTcpServer * server,
								 guint local_port);

gboolean gtcp_server_get_use_ssl (GTcpServer * server);
gboolean gtcp_server_set_use_ssl (GTcpServer * server,
								  gboolean use_ssl);

gint gtcp_server_get_max_connections (GTcpServer * server);
void gtcp_server_set_max_connections (GTcpServer * server,
									  gint max_connections);

GTcpServerKillStyle gtcp_server_get_kill_style (GTcpServer * server);
void gtcp_server_set_kill_style (GTcpServer * server,
								 GTcpServerKillStyle kill_style);

gboolean gtcp_server_get_do_reverse_lookups (GTcpServer * server);
void gtcp_server_set_do_reverse_lookups (GTcpServer * server,
										 gboolean do_reverse_lookups);

gsize gtcp_server_get_buffer_size (GTcpServer * server);
void gtcp_server_set_buffer_size (GTcpServer * server,
								  gsize buffer_size);

gulong gtcp_server_get_bytes_read (GTcpServer * server);
gulong gtcp_server_get_bytes_written (GTcpServer * server);


G_END_DECLS

#endif /* __GTCP_SERVER_H__ */
