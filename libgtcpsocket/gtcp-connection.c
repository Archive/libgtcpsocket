/*
 * LibGTcpSocket: libgtcpsocket/gtcp-connection.c
 *
 * Copyright (C) 2001-2002 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * 
 * GLib 2.0/GObject-based TCP/IP networking sockets wrapper.
 * 
 * Notes on editing: Tab size: 4 
 */


#include "gtcp-connection.h"
#include "gtcp-socket-type-builtins.h"
#include "gtcp-i18n.h"

#include "marshal.h"
#include "gtcp-proxy-private.h"
#include "gtcp-dns.h"

#include <config.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>


#ifndef GTCP_SOCKET_DEFAULT_BUFFER_SIZE
#	define GTCP_SOCKET_DEFAULT_BUFFER_SIZE 2048
#endif /* GTCP_SOCKET_DEFAULT_BUFFER_SIZE */


#ifdef USE_SSL
#	ifdef USE_OPENSSL

#include <openssl/ssl.h>

#	elif defined USE_GNUTLS

#include <gnutls/gnutls.h>

#	endif /* USE_GNUTLS, USE_OPENSSL */
#endif /* USE_SSL */


enum
{
	PROP_0,
	/* User-settable */
	PROP_ADDRESS,
	PROP_PORT,
	PROP_LOCAL_ADDRESS,
	PROP_LOCAL_PORT,

	PROP_CONN_STYLE,
	PROP_USE_SSL,
	PROP_BUFFER_SIZE,

	/* Considered read-only */
	PROP_IP_ADDRESS,
	PROP_STATUS,
	PROP_BYTES_READ,
	PROP_BYTES_WRITTEN,

	/* Private to GTcpSocket */
	PROP_IS_SERVER,
	PROP_SOCKET_FD
};

enum
{
	LOOKUP_DONE,
	CONNECT_DONE,
	RECV,
	SEND,
	CLOSED,
	LAST_SIGNAL
};


typedef gboolean (*GTcpWatchFunc) (GIOCondition cond,
								   gpointer user_data);


typedef struct
{
	gint id;
	gpointer arg1;
	gpointer arg2;
}
GTcpSignal;


typedef struct
{
	guchar *data;
	gssize length;
}
GTcpBuffer;


typedef struct
{
	GSource source;

	GPollFD pollfd;
	GIOCondition condition;
	GTcpWatchFunc callback;
}
GTcpWatch;


struct _GTcpConnectionPrivate
{
	/* Object properties */
	gchar *address;
	guint port;

	gchar *local_address;
	guint local_port;

	GTcpConnectionStyle conn_style:2;
	gboolean use_ssl:1;

	/* Internal property, used by GTcpServer */
	gboolean is_server:1;

	/* Read-only properties */
	GTcpConnectionStatus status:4;
	guint buffer_size;
	gulong bytes_read;
	gulong bytes_written;
	gchar *ip_address;

	/* Used for dns lookups. */
	GTcpDNSHandle dns_handle;

	/* Object Data */
	GTcpProxy *proxy;
	GTcpDNSEntry *proxy_entry;
	GTcpDNSEntry *entry;

	/* Threading */
	GMainContext *io_context;
	GMainLoop *io_mainloop;
	GAsyncQueue *write_queue;
	GAsyncQueue *signal_queue;

	/* GSource to poll signal_queue */
	GSource *source;
	gint signal_id;

	/* The socket itself :-) */
	gint sockfd;

	/* SSL */
#ifdef USE_SSL
#	ifdef USE_OPENSSL

	SSL *ssl;

#	elif defined USE_GNUTLS

	gnutls_session session;
	gnutls_certificate_credentials xcred;

#	endif						/* USE_OPENSSL, USE_GNUTLS */
#endif							/* USE_SSL */
};


/* ****************** *
 *  Global Variables  *
 * ****************** */

static gpointer parent_class = NULL;
static gint gtcp_connection_signals[LAST_SIGNAL] = { 0 };

#ifdef USE_SSL
#	ifdef USE_OPENSSL

static SSL_METHOD *ssl_method = NULL;
static SSL_CTX *ssl_context = NULL;

#	elif defined USE_GNUTLS

static const gint protocol_priority[] = {
	GNUTLS_TLS1,
	GNUTLS_SSL3,
	0
};

static const gint cipher_priority[] = {
	GNUTLS_CIPHER_RIJNDAEL_128_CBC,
	GNUTLS_CIPHER_3DES_CBC,
	GNUTLS_CIPHER_RIJNDAEL_256_CBC,
	GNUTLS_CIPHER_ARCFOUR,
	0
};

static const gint comp_priority[] = {
	GNUTLS_COMP_ZLIB,
	GNUTLS_COMP_NULL,
	0
};

static const gint kx_priority[] = {
	GNUTLS_KX_DHE_RSA,
	GNUTLS_KX_RSA,
	GNUTLS_KX_DHE_DSS,
	0
};

static const gint mac_priority[] = {
	GNUTLS_MAC_SHA,
	GNUTLS_MAC_MD5,
	0
};

#	endif /* USE_OPENSSL */
#endif /* USE_SSL */


/* ************************* *
 *  Connection Read Polling  *
 * ************************* */

static gboolean
source_prepare (GSource * source,
				gint * timeout)
{
	*timeout = -1;

	return FALSE;
}


static gboolean
source_check (GSource * source)
{
	GTcpWatch *watch = (GTcpWatch *) source;

	return watch->pollfd.revents & watch->condition;
}


static gboolean
source_dispatch (GSource * source,
				 GSourceFunc callback,
				 gpointer user_data)
{
	GTcpWatchFunc func;
	GTcpWatch *watch = (GTcpWatch *) source;

	if (callback == NULL)
		g_error ("No callback");

	func = (GTcpWatchFunc) callback;

	return (*func) (watch->pollfd.revents & watch->condition, user_data);
}


static void
source_finalize (GSource * source)
{
}


static const GSourceFuncs sourcefuncs = {
	source_prepare,
	source_check,
	source_dispatch,
	source_finalize
};


/* ************************* *
 *  Basic Utility Functions  *
 * ************************* */

static void
set_ip_address (GTcpConnection * conn,
				const gchar * ip_address)
{
	if (conn->_priv->ip_address)
		g_free (conn->_priv->ip_address);

	conn->_priv->ip_address = g_strdup (ip_address);
}


/* ********************** *
 *  Threaded IO Handlers  *
 * ********************** */

static gpointer
write_thread (gpointer data)
{
	GTcpConnection *conn = GTCP_CONNECTION (data);
	GAsyncQueue *queue = conn->_priv->write_queue;
	GTcpBuffer *buffer = NULL;
	guint i = 0;

	if (data == NULL || conn->_priv->sockfd < 0)
	{
		g_thread_exit (NULL);
		return NULL;
	}

	while (TRUE)
	{
		GTcpSignal *signal = NULL;
		gssize bytes_written = 0,
		  total_bytes_written = 0;

		buffer = g_async_queue_pop (queue);

		i = i + 1;

		if (buffer != NULL)
		{
			/* A null data arg tells us we're disconnecting, so leave. */
			if (buffer->data == NULL)
			{
				g_free (buffer);
				buffer = NULL;
				break;
			}

			while (total_bytes_written < buffer->length && conn->_priv->sockfd > 0)
			{
#ifdef USE_SSL
				if (conn->_priv->use_ssl)
				{
#	ifdef USE_OPENSSL
					bytes_written = SSL_write (conn->_priv->ssl,
											   buffer->data + total_bytes_written,
											   buffer->length - total_bytes_written);
#	elif defined USE_GNUTLS
					bytes_written =
						gnutls_record_send (conn->_priv->session,
											buffer->data + total_bytes_written,
											buffer->length - total_bytes_written);
#	endif /* USE_OPENSSL || USE_GNUTLS */
				}
				else
#endif /* USE_SSL */
				{
					bytes_written = send (conn->_priv->sockfd,
										  buffer->data + total_bytes_written,
										  buffer->length - total_bytes_written,
										  MSG_NOSIGNAL);
				}

				if (bytes_written < 0)
				{
					g_free (buffer->data);
					g_free (buffer);
					buffer = NULL;
					goto out;
				}
				else
				{
					total_bytes_written += bytes_written;
				}
			}

			signal = g_new0 (GTcpSignal, 1);
			g_assert (signal != NULL);

			signal->id = gtcp_connection_signals[SEND];
			signal->arg1 = buffer->data;
			signal->arg2 = GINT_TO_POINTER (total_bytes_written);

			g_async_queue_push (conn->_priv->signal_queue, signal);

			g_free (buffer);
			buffer = NULL;
		}
		else
		{
			g_assert_not_reached ();
		}
	}

  out:
	while (g_async_queue_length (queue) > 0)
	{
		buffer = g_async_queue_pop (queue);

		if (buffer != NULL)
		{
			if (buffer->data != NULL)
				g_free (buffer->data);

			g_free (buffer);
		}
	}
	g_async_queue_unref (queue);

	g_thread_exit (NULL);
	return NULL;
}


static gboolean
handle_incoming (GIOCondition cond,
				 gpointer data)
{
	GTcpConnection *conn = GTCP_CONNECTION (data);
	guchar *buffer;
	gssize length;
	GTcpSignal *signal = NULL;

	switch (cond)
	{
		/* There is data to be read */
	case G_IO_PRI:
	case G_IO_IN:
		buffer = g_new0 (guchar, conn->_priv->buffer_size);
		g_assert (buffer != NULL);

#ifdef USE_SSL
		if (conn->_priv->use_ssl)
		{
#	ifdef USE_OPENSSL
			length = SSL_read (conn->_priv->ssl, buffer, conn->_priv->buffer_size);

			if (length <= 0)
			{
				gint ssl_error;

				ssl_error = SSL_get_error (conn->_priv->ssl, length);
				switch (ssl_error)
				{
					/* Connection is dead */
				case SSL_ERROR_ZERO_RETURN:
					signal = g_new0 (GTcpSignal, 1);
					g_assert (signal != NULL);

					signal->id = gtcp_connection_signals[CLOSED];
					signal->arg1 = GINT_TO_POINTER (FALSE);
					signal->arg2 = NULL;

					g_async_queue_push (conn->_priv->signal_queue, signal);
					g_free (buffer);
					return FALSE;
					break;

					/* Some other error */
				default:
					break;
				}
			}
#	elif defined USE_GNUTLS
			length = gnutls_record_recv (conn->_priv->session, buffer,
										 conn->_priv->buffer_size);
			if (length <= 0)
			{
				signal = g_new0 (GTcpSignal, 1);
				g_assert (signal != NULL);

				signal->id = gtcp_connection_signals[CLOSED];
				signal->arg1 = GINT_TO_POINTER (FALSE);
				signal->arg2 = NULL;

				g_async_queue_push (conn->_priv->signal_queue, signal);
				g_free (buffer);
				return FALSE;
				break;
			}
#	endif /* USE_OPENSSL, USE_GNUTLS */
		}
		else
#endif /* USE_SSL */
		{
			length = recv (conn->_priv->sockfd, buffer, conn->_priv->buffer_size,
						   MSG_NOSIGNAL);

			if (length <= 0)
			{
				signal = g_new0 (GTcpSignal, 1);
				g_assert (signal != NULL);

				signal->id = gtcp_connection_signals[CLOSED];
				signal->arg1 = GINT_TO_POINTER (FALSE);
				signal->arg2 = NULL;

				g_async_queue_push (conn->_priv->signal_queue, signal);

				g_free (buffer);
				return FALSE;
			}
		}
		break;

		/* Socket is gone */
	case G_IO_HUP:
	case G_IO_ERR:
		signal = g_new0 (GTcpSignal, 1);
		g_assert (signal != NULL);

		signal->id = gtcp_connection_signals[CLOSED];
		signal->arg1 = GINT_TO_POINTER (FALSE);
		signal->arg2 = NULL;

		g_async_queue_push (conn->_priv->signal_queue, signal);
		return FALSE;
		break;

	default:
		return TRUE;
		break;
	}

	signal = g_new0 (GTcpSignal, 1);

	g_assert (signal != NULL);

	signal->id = gtcp_connection_signals[RECV];
	signal->arg1 = buffer;
	signal->arg2 = GINT_TO_POINTER (length);

	g_async_queue_push (conn->_priv->signal_queue, signal);

	return TRUE;
}


/* ************************** *
 *  Main Threading Functions  *
 * ************************** */

/*
 * This pushes out the connect-done signal & runs the io_mainloop
 */
static void
io_main_thread (GTcpConnection * conn,
				GTcpConnectionStatus status)
{
	GTcpSignal *signal = NULL;

	signal = g_new0 (GTcpSignal, 1);
	g_assert (signal != NULL);

	signal->id = gtcp_connection_signals[CONNECT_DONE];

	if (conn->_priv->status > GTCP_CONNECTION_CLOSED)
	{
		signal->arg1 = GINT_TO_POINTER (status);
		signal->arg2 = NULL;
		g_async_queue_push (conn->_priv->signal_queue, signal);

		if (status == GTCP_CONNECTION_CONNECTED)
		{
			g_main_loop_run (conn->_priv->io_mainloop);
		}
	}
}


/* This starts up the server connection, and if it succeeds, it runs the io mainloop,
 * blocking until the connection is closed. This is in it's own thread, so it doesn't
 * matter.
 */
static gpointer
open_server_thread (gpointer thread_data)
{
	GTcpConnection *conn = GTCP_CONNECTION (thread_data);
	GTcpConnectionStatus status = GTCP_CONNECTION_CONNECTING;

#ifdef USE_SSL
	if (conn->_priv->use_ssl)
	{
		GTcpSignal *signal = NULL;

#	ifdef USE_OPENSSL
		if (SSL_set_fd (conn->_priv->ssl, conn->_priv->sockfd) == 0)
		{
			signal = g_new0 (GTcpSignal, 1);
			signal->id = gtcp_connection_signals[CLOSED];
			signal->arg1 = signal->arg2 = NULL;

			g_async_queue_push (conn->_priv->signal_queue, signal);
			return NULL;
		}

		if (SSL_accept (conn->_priv->ssl) < 1)
		{
			signal = g_new0 (GTcpSignal, 1);
			signal->id = gtcp_connection_signals[CLOSED];
			signal->arg1 = signal->arg2 = NULL;

			g_async_queue_push (conn->_priv->signal_queue, signal);
			return NULL;
		}
#	elif defined USE_GNUTLS
		gint err;

		gnutls_init (&(conn->_priv->session), GNUTLS_SERVER);

		gnutls_transport_set_ptr (conn->_priv->session,
								  (gnutls_transport_ptr) conn->_priv->sockfd);

		gnutls_protocol_set_priority (conn->_priv->session, protocol_priority);
		gnutls_cipher_set_priority (conn->_priv->session, cipher_priority);
		gnutls_compression_set_priority (conn->_priv->session, comp_priority);
		gnutls_kx_set_priority (conn->_priv->session, kx_priority);
		gnutls_mac_set_priority (conn->_priv->session, mac_priority);

		gnutls_cred_set (conn->_priv->session, GNUTLS_CRD_CERTIFICATE,
						 conn->_priv->xcred);

		err = gnutls_handshake (conn->_priv->session);

		while (err == GNUTLS_E_AGAIN || err == GNUTLS_E_INTERRUPTED)
		{
			err = gnutls_handshake (conn->_priv->session);
		}

		if (err < 0)
		{
			gnutls_certificate_free_credentials (conn->_priv->xcred);
			gnutls_deinit (conn->_priv->session);

			signal = g_new0 (GTcpSignal, 1);
			signal->id = gtcp_connection_signals[CLOSED];
			signal->arg1 = signal->arg2 = NULL;

			g_async_queue_push (conn->_priv->signal_queue, signal);
			return NULL;
		}
#	endif /* USE_OPENSSL, USE_GNUTLS */
	}
#endif /* USE_SSL */

	io_main_thread (conn, status);

	return NULL;
}


/*
 * This starts up the connection, and if it succeeds, it runs the io mainloop, blocking
 * until the connection is closed. This is in it's own thread, so it doesn't matter.
 */
static gpointer
open_connection_thread (gpointer thread_data)
{
	GTcpConnection *conn = GTCP_CONNECTION (thread_data);
	GTcpConnectionStatus status = GTCP_CONNECTION_CONNECTING;
	GSList *current_addr = NULL,
	 *proxied_addr = NULL;
	gint connect_retval;
	struct sockaddr_in sin;

	if (conn->_priv->proxy != NULL)
	{
		current_addr = conn->_priv->proxy_entry->ip_addresses;
		proxied_addr = conn->_priv->entry->ip_addresses;
	}
	else
	{
		current_addr = conn->_priv->entry->ip_addresses;
		proxied_addr = current_addr;
	}

	/* Keep trying until we're told to stop, run out of addresses, or suceed in
	   connecting */
	while (conn->_priv->status == GTCP_CONNECTION_CONNECTING
		   && current_addr != NULL
		   && status != GTCP_CONNECTION_CONNECTED && proxied_addr != NULL)
	{
		if (conn->_priv->sockfd != -1)
		{
			shutdown (conn->_priv->sockfd, SHUT_RDWR);
			conn->_priv->sockfd = -1;
		}

		conn->_priv->sockfd = socket (PF_INET, SOCK_STREAM, 0);

		memset (&sin, 0, sizeof (sin));

		inet_aton ((gchar *) (current_addr->data), &(sin.sin_addr));

		if (conn->_priv->proxy == NULL)
		{
			if (conn->_priv->ip_address != NULL)
				g_free (conn->_priv->ip_address);
			conn->_priv->ip_address = g_strdup (current_addr->data);
		}

		sin.sin_family = AF_INET;
		sin.sin_port = g_htons (conn->_priv->port);

		errno = 0;
		connect_retval = connect (conn->_priv->sockfd, (struct sockaddr *) &(sin),
								  sizeof (sin));

		/* And here's where it gets sick :-o */
		if (connect_retval == 0)
		{
#ifdef USE_SSL
			if (conn->_priv->use_ssl)
			{
				gint err;

#	ifdef USE_OPENSSL
				conn->_priv->ssl = SSL_new (ssl_context);
				SSL_set_fd (conn->_priv->ssl, conn->_priv->sockfd);
				err = SSL_connect (conn->_priv->ssl);

				if (err != 1)
				{
					SSL_shutdown (conn->_priv->ssl);
					SSL_free (conn->_priv->ssl);
					conn->_priv->ssl = NULL;
					status = GTCP_CONNECTION_ERROR_INTERNAL;

					goto out;
				}
#	elif defined USE_GNUTLS
				gnutls_init (&(conn->_priv->session), GNUTLS_CLIENT);

				gnutls_transport_set_ptr (conn->_priv->session,
										  (gnutls_transport_ptr) conn->_priv->sockfd);

				gnutls_protocol_set_priority (conn->_priv->session, protocol_priority);
				gnutls_cipher_set_priority (conn->_priv->session, cipher_priority);
				gnutls_compression_set_priority (conn->_priv->session, comp_priority);
				gnutls_kx_set_priority (conn->_priv->session, kx_priority);
				gnutls_mac_set_priority (conn->_priv->session, mac_priority);

				gnutls_cred_set (conn->_priv->session, GNUTLS_CRD_CERTIFICATE,
								 conn->_priv->xcred);

				err = gnutls_handshake (conn->_priv->session);

				while (err == GNUTLS_E_AGAIN || err == GNUTLS_E_INTERRUPTED)
				{
					err = gnutls_handshake (conn->_priv->session);
				}

				if (err < 0)
				{
					gnutls_certificate_free_credentials (conn->_priv->xcred);
					gnutls_deinit (conn->_priv->session);
					status = GTCP_CONNECTION_ERROR_INTERNAL;

					goto out;
				}
#	endif /* USE_OPENSSL || USE_GNUTLS */
			}
#endif /* USE_SSL */
			status = GTCP_CONNECTION_CONNECTED;

			/* Proxy traversal */
			if (conn->_priv->proxy != NULL)
			{
				if (conn->_priv->ip_address != NULL)
					g_free (conn->_priv->ip_address);
				conn->_priv->ip_address = proxied_addr->data;

				status = _gtcp_proxy_traverse_proxy (conn->_priv->proxy,
													 proxied_addr->data,
													 conn->_priv->port,
													 conn->_priv->sockfd, FALSE);

				if (status != GTCP_CONNECTION_CONNECTED)
					proxied_addr = proxied_addr->next;
			}
			/* FIXME: Implement attempting to connect through the proxy here */
		}
		else if (connect_retval != 0 && conn->_priv->proxy != NULL)
		{
			status = GTCP_CONNECTION_ERROR_PROXY_ERROR;
			current_addr = current_addr->next;
		}
		else
		{
			switch (errno)
			{
				/* If we got some error. */
			case ECONNREFUSED:
				status = GTCP_CONNECTION_ERROR_CONNECTION_REFUSED;
				break;
			case ETIMEDOUT:
				status = GTCP_CONNECTION_ERROR_TIMEOUT;
				break;
			case ENETUNREACH:
				status = GTCP_CONNECTION_ERROR_NETWORK_UNREACHABLE;
				break;
			case EACCES:
			case EPERM:
				status = GTCP_CONNECTION_ERROR_BAD_BROADCAST_OR_FIREWALL;
				break;
			default:
				status = GTCP_CONNECTION_ERROR_INTERNAL;
				break;
			}

			/* If connect_retval != 0, go on to the next address. If
			 * connect_retval == 0, then the basic outgoing connection 
			 * succeeded, and the only time we'd mess with current_addr again
			 * is if we are having problems beyond the proxy, in which case, we
			 * want to stick with the proxy address that we know works, rather
			 * than trying secondary addresses for the proxy. Uhh... yeah.
			 */
			current_addr = current_addr->next;
		}
	}

	/* Yeah, yeah, I know... */
#ifdef USE_SSL
  out:
#endif /* USE_SSL */
	io_main_thread (conn, status);

	return NULL;
}


/* ******************************** *
 *  Intermediate Utility Functions  *
 * ******************************** */

/* This function runs every 50ms looking for new signals */
static gboolean
check_for_signals (gpointer user_data)
{
	GTcpConnection *conn = GTCP_CONNECTION (user_data);
	GTcpSignal *signal;

	if (conn->_priv->signal_queue == NULL)
		return FALSE;

	signal = (GTcpSignal *) g_async_queue_try_pop (conn->_priv->signal_queue);

	if (signal == NULL)
		return TRUE;

	/* Yes, yes, I know what you're thinking: "Why not just use a switch
	 * statement?" Well, I did that originally, but gcc 3.1 was giving me
	 * compiler warnings, and being the -Wall -Werror sicko I am, I
	 * switched to this. If you know a way around this, feel free to let
	 * me know :-) 
	 */
	if (signal->id == gtcp_connection_signals[CONNECT_DONE])
	{
		g_signal_emit (user_data, signal->id, 0, GPOINTER_TO_INT (signal->arg1));
	}
	else if (signal->id == gtcp_connection_signals[RECV]
			 || signal->id == gtcp_connection_signals[SEND])
	{
		if (conn->_priv->status > GTCP_CONNECTION_CLOSED)
		{
			g_signal_emit (user_data, signal->id, 0, signal->arg1,
						   GPOINTER_TO_INT (signal->arg2));
		}

		g_free (signal->arg1);
	}
	else if (signal->id == gtcp_connection_signals[CLOSED])
	{
		g_signal_emit (user_data, signal->id, 0, GPOINTER_TO_INT (signal->arg1));
	}
	else
	{
		g_free (signal);
		return FALSE;
	}

	g_free (signal);
	return TRUE;
}


static void
start_connection_process (GTcpConnection * conn)
{
	GThread *io_thread = NULL;

	if (conn->_priv->signal_queue == NULL)
		conn->_priv->signal_queue = g_async_queue_new ();
	if (conn->_priv->write_queue == NULL)
		conn->_priv->write_queue = g_async_queue_new ();

	conn->_priv->signal_id = g_timeout_add (50, check_for_signals, conn);

	if (!g_thread_supported ())
		g_thread_init (NULL);

	if (conn->_priv->io_context == NULL)
		conn->_priv->io_context = g_main_context_new ();

	if (conn->_priv->io_mainloop == NULL)
		conn->_priv->io_mainloop = g_main_loop_new (conn->_priv->io_context, FALSE);

	if (conn->_priv->is_server)
		io_thread = g_thread_create (open_server_thread, conn, FALSE, NULL);
	else
		io_thread = g_thread_create (open_connection_thread, conn, FALSE, NULL);

	if (io_thread == NULL)
	{
		g_signal_emit (G_OBJECT (conn), gtcp_connection_signals[CONNECT_DONE], 0,
					   GTCP_CONNECTION_ERROR_THREAD_ERROR);
		return;
	}
}


static GTcpSendStatus
append_data_to_queue (GTcpConnection * conn,
					  gpointer data,
					  gsize length)
{
	GTcpBuffer *buffer;
	gsize real_length = length,
	  done_length = 0;

	while (real_length > 0)
	{
		buffer = g_new0 (GTcpBuffer, 1);

		if (real_length >= conn->_priv->buffer_size)
		{
			buffer->data = (guchar *) data + done_length;
			buffer->length = conn->_priv->buffer_size;

			real_length -= conn->_priv->buffer_size;
			done_length += conn->_priv->buffer_size;
		}
		else
		{
			buffer->data = (guchar *) data + done_length;
			buffer->length = real_length;

			real_length = 0;
		}

		if (conn->_priv->status == GTCP_CONNECTION_CONNECTED)
			g_async_queue_push (conn->_priv->write_queue, buffer);
		else
			return GTCP_SEND_ERROR;
	}

	return GTCP_SEND_DATA_QUEUED;
}


/* **************************** *
 *  GTcpDNS Callback Functions  *
 * **************************** */

static void
dns_callback (const GTcpDNSEntry * entry,
			  gpointer data)
{
	GTcpConnection *conn = GTCP_CONNECTION (data);

	g_assert (entry != NULL);
	g_assert (conn != NULL);

	if (conn->_priv->entry != NULL)
	{
		gtcp_dns_entry_free (conn->_priv->entry);
		conn->_priv->entry = NULL;
	}

	conn->_priv->entry = gtcp_dns_entry_copy (entry);

	if (conn->_priv->status > GTCP_CONNECTION_CLOSED)
	{
		g_signal_emit (G_OBJECT (conn), gtcp_connection_signals[LOOKUP_DONE], 0,
					   entry->status);
	}

}


static void
proxy_dns_callback (const GTcpDNSEntry * orig_entry,
					gpointer data)
{
	GTcpConnection *conn = GTCP_CONNECTION (data);
	GTcpDNSEntry *entry = gtcp_dns_entry_copy (orig_entry);
	struct in_addr addr;

	g_assert (entry != NULL);
	g_assert (conn != NULL);

	if (conn->_priv->proxy_entry != NULL)
	{
		gtcp_dns_entry_free (conn->_priv->proxy_entry);
		conn->_priv->proxy_entry = NULL;
	}

	conn->_priv->proxy_entry = entry;

	if (entry->status != GTCP_LOOKUP_OK)
	{
		if (conn->_priv->status > GTCP_CONNECTION_CLOSED)
		{
			g_signal_emit (G_OBJECT (conn),
						   gtcp_connection_signals[LOOKUP_DONE], 0, entry->status);
		}
	}
	else if (conn->_priv->status > GTCP_CONNECTION_CLOSED)
	{
		/*
		 * FIXME: This sucks, since we're serializing these lookups, when
		 * they could be done at the same time -- I'm just not sure how to 
		 * do it nicely... 
		 */
		if (inet_aton (conn->_priv->address, &addr))
		{
			entry = g_new0 (GTcpDNSEntry, 1);
			g_assert (entry != NULL);

			entry->status = GTCP_LOOKUP_OK;
			entry->hostname = NULL;
			entry->aliases = NULL;
			entry->ip_addresses = g_slist_append (entry->ip_addresses,
												  conn->_priv->address);
			dns_callback (entry, conn);
		}
		else
		{
			conn->_priv->dns_handle = gtcp_dns_get (conn->_priv->address,
													dns_callback, conn);
		}
	}
}


/* ************************** *
 *  GTcpConnection Callbacks  *
 * ************************** */

static void
gtcp_connection_lookup_done_handler (GTcpConnection * conn,
									 GTcpLookupStatus status)
{
	conn->_priv->dns_handle = GTCP_DNS_INVALID_HANDLE;

	/* The lookup failed, or we're a server & this connection is already established
	   In either case, don't do anything */
	if (status != GTCP_LOOKUP_OK || conn->_priv->is_server)
		return;

	start_connection_process (conn);
}


static void
gtcp_connection_connect_done_handler (GTcpConnection * conn,
									  GTcpConnectionStatus status)
{
	GSource *source = NULL;
	GTcpWatch *watch;

	conn->_priv->status = status;
	g_object_freeze_notify (G_OBJECT (conn));
	g_object_notify (G_OBJECT (conn), "status");
	g_object_notify (G_OBJECT (conn), "socket-fd");
	g_object_thaw_notify (G_OBJECT (conn));

	/* The connection failed, don't do anything else */
	if (status != GTCP_CONNECTION_CONNECTED)
		return;

	source = g_source_new ((GSourceFuncs *) & sourcefuncs, sizeof (GTcpWatch));
	g_assert (source != NULL);

	watch = (GTcpWatch *) source;

	watch->pollfd.fd = conn->_priv->sockfd;
	watch->pollfd.events = watch->condition = (G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP);

	g_source_set_can_recurse (source, FALSE);

	g_source_add_poll (source, &watch->pollfd);

	g_source_set_callback (source, (GSourceFunc) handle_incoming, conn, NULL);
	g_source_attach (source, conn->_priv->io_context);

	conn->_priv->source = source;

	g_thread_create (write_thread, conn, FALSE, NULL);
}


static void
gtcp_connection_send_handler (GTcpConnection * conn,
							  gconstpointer data,
							  gsize length)
{
	conn->_priv->bytes_written += length;
	g_object_notify (G_OBJECT (conn), "bytes-written");
}


static void
gtcp_connection_recv_handler (GTcpConnection * conn,
							  gconstpointer data,
							  gsize length)
{
	conn->_priv->bytes_read += length;
	g_object_notify (G_OBJECT (conn), "bytes-read");
}


static void
gtcp_connection_closed_handler (GTcpConnection * conn,
								gboolean requested)
{
	GTcpBuffer *write_buf = NULL;

	conn->_priv->status = GTCP_CONNECTION_CLOSED;

	if (conn->_priv->dns_handle != GTCP_DNS_INVALID_HANDLE)
	{
		gtcp_dns_cancel (conn->_priv->dns_handle);
		conn->_priv->dns_handle = GTCP_DNS_INVALID_HANDLE;
	}

	if (conn->_priv->source != NULL)
	{
		g_source_destroy (conn->_priv->source);
		conn->_priv->source = NULL;
	}

	g_main_loop_quit (conn->_priv->io_mainloop);

	write_buf = g_new0 (GTcpBuffer, 1);
	g_assert (write_buf != NULL);

	write_buf->data = NULL;
	write_buf->length = 0;

	g_async_queue_push (conn->_priv->write_queue, write_buf);
	conn->_priv->write_queue = NULL;

	if (conn->_priv->signal_id != -1)
	{
		g_source_remove (conn->_priv->signal_id);
		conn->_priv->signal_id = -1;
	}

	/* Close the socket if we're open */
	if (conn->_priv->sockfd >= 0)
	{
#ifdef USE_SSL
		if (conn->_priv->use_ssl)
		{
#	ifdef USE_OPENSSL
			if (conn->_priv->ssl != NULL)
			{
				SSL_shutdown (conn->_priv->ssl);
				SSL_free (conn->_priv->ssl);
			}
#	elif defined USE_GNUTLS
			gnutls_bye (conn->_priv->session, GNUTLS_SHUT_RDWR);
			gnutls_certificate_free_credentials (conn->_priv->xcred);
			gnutls_deinit (conn->_priv->session);
#	endif /* USE_OPENSSL, USE_GNUTLS */
		}
#endif /* USE_SSL */
		shutdown (conn->_priv->sockfd, SHUT_RDWR);
		conn->_priv->sockfd = -1;
	}
}


/* ******************* *
 *  GObject Callbacks  *
 * ******************* */

static void
gtcp_connection_get_property (GObject * object,
							  guint property,
							  GValue * value,
							  GParamSpec * param_spec)
{
	GTcpConnection *conn = GTCP_CONNECTION (object);

	switch (property)
	{
	case PROP_ADDRESS:
		g_value_set_string (value, conn->_priv->address);
		break;
	case PROP_PORT:
		g_value_set_uint (value, conn->_priv->port);
		break;
	case PROP_LOCAL_ADDRESS:
		g_value_set_string (value, conn->_priv->local_address);
		break;
	case PROP_LOCAL_PORT:
		g_value_set_uint (value, conn->_priv->local_port);
		break;
	case PROP_CONN_STYLE:
		g_value_set_enum (value, conn->_priv->conn_style);
		break;
	case PROP_USE_SSL:
		g_value_set_boolean (value, conn->_priv->use_ssl);
		break;

	case PROP_STATUS:
		g_value_set_enum (value, conn->_priv->status);
		break;

	case PROP_IP_ADDRESS:
		g_value_set_string (value, conn->_priv->ip_address);
		break;
	case PROP_IS_SERVER:
		g_value_set_boolean (value, conn->_priv->is_server);
		break;

	case PROP_BUFFER_SIZE:
		g_value_set_uint (value, conn->_priv->buffer_size);
		break;

	case PROP_BYTES_READ:
		g_value_set_ulong (value, conn->_priv->bytes_read);
		break;
	case PROP_BYTES_WRITTEN:
		g_value_set_ulong (value, conn->_priv->bytes_written);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}


static void
gtcp_connection_set_property (GObject * object,
							  guint property,
							  const GValue * value,
							  GParamSpec * param_spec)
{
	GTcpConnection *conn = GTCP_CONNECTION (object);

	switch (property)
	{
	case PROP_ADDRESS:
		gtcp_connection_set_address (conn, g_value_get_string (value));
		break;
	case PROP_PORT:
		gtcp_connection_set_port (conn, g_value_get_uint (value));
		break;
	case PROP_LOCAL_ADDRESS:
		if (conn->_priv->local_address)
			g_free (conn->_priv->local_address);

		conn->_priv->local_address = g_strdup (g_value_get_string (value));
		g_object_notify (object, "local-address");
		break;
	case PROP_LOCAL_PORT:
		conn->_priv->local_port = g_value_get_uint (value);
		g_object_notify (object, "local-port");
		break;
	case PROP_CONN_STYLE:
		gtcp_connection_set_conn_style (conn, g_value_get_enum (value));
		break;
	case PROP_USE_SSL:
		gtcp_connection_set_use_ssl (conn, g_value_get_boolean (value));
		break;

	case PROP_IP_ADDRESS:
		set_ip_address (conn, g_value_get_string (value));
		break;
	case PROP_STATUS:
		conn->_priv->status = g_value_get_enum (value);
		g_object_notify (object, "status");
		break;
	case PROP_IS_SERVER:
		conn->_priv->is_server = g_value_get_boolean (value);
		g_object_notify (object, "is-server");
		break;
	case PROP_SOCKET_FD:
		g_return_if_fail (conn->_priv->is_server != FALSE);

		conn->_priv->sockfd = g_value_get_int (value);
		g_object_notify (object, "socket-fd");

		start_connection_process (GTCP_CONNECTION (conn));
		break;

	case PROP_BUFFER_SIZE:
		gtcp_connection_set_buffer_size (conn, g_value_get_uint (value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}


static void
gtcp_connection_finalize (GObject * obj)
{
	GTcpConnection *conn = GTCP_CONNECTION (obj);

	if (conn->_priv->status > GTCP_CONNECTION_CLOSED)
	{
		gtcp_connection_close (conn);
	}

	if (conn->_priv->address)
	{
		g_free (conn->_priv->address);
		conn->_priv->address = NULL;
	}

	if (conn->_priv->local_address != NULL)
	{
		g_free (conn->_priv->local_address);
		conn->_priv->local_address = NULL;
	}

	if (conn->_priv->ip_address)
	{
		g_free (conn->_priv->ip_address);
		conn->_priv->ip_address = NULL;
	}

	if (conn->_priv->entry != NULL)
		gtcp_dns_entry_free (conn->_priv->entry);

	if (conn->_priv->proxy_entry != NULL)
		gtcp_dns_entry_free (conn->_priv->proxy_entry);

	if (conn->_priv->signal_queue != NULL)
	{
		g_async_queue_unref (conn->_priv->signal_queue);
		conn->_priv->signal_queue = NULL;
	}

	if (conn->_priv->io_context != NULL)
	{
		g_main_context_unref (conn->_priv->io_context);
		conn->_priv->io_context = NULL;
	}

	g_free (conn->_priv);
	conn->_priv = NULL;

	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (obj);
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gtcp_connection_base_init (gpointer class)
{
#ifdef USE_SSL
#	ifdef USE_OPENSSL

	SSL_library_init ();
	SSLeay_add_ssl_algorithms ();

	if (ssl_method == NULL)
		ssl_method = SSLv23_method ();
	if (ssl_context == NULL)
		ssl_context = SSL_CTX_new (ssl_method);

#	elif defined USE_GNUTLS

	gnutls_global_init ();

#	endif /* USE_OPENSSL, USE_GNUTLS */
#endif /* USE_SSL */

	_gtcp_proxy_initialize ();
}


static void
gtcp_connection_base_finalize (gpointer class)
{
	_gtcp_proxy_shutdown ();
}


static void
gtcp_connection_class_init (GTcpConnectionClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = gtcp_connection_get_property;
	object_class->set_property = gtcp_connection_set_property;
	object_class->finalize = gtcp_connection_finalize;

	class->lookup_done = gtcp_connection_lookup_done_handler;
	class->connect_done = gtcp_connection_connect_done_handler;
	class->send = gtcp_connection_send_handler;
	class->recv = gtcp_connection_recv_handler;
	class->closed = gtcp_connection_closed_handler;

	g_object_class_install_property (object_class, PROP_ADDRESS,
									 g_param_spec_string
									 ("address", _("Address"),
									  _("The hostname or IP address to connect to."),
									  NULL, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_PORT,
									 g_param_spec_uint
									 ("port", _("Port Number"),
									  _("The port number to connect to."), 0, 65535, 0,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_LOCAL_ADDRESS,
									 g_param_spec_string
									 ("local-address", _("Local Address"),
									  _("The hostname or IP address of this computer."),
									  NULL, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_LOCAL_PORT,
									 g_param_spec_uint
									 ("local-port", _("Local Port Number"),
									  _("The local port number to connect through."),
									  0, 65535, 0,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_CONN_STYLE,
									 g_param_spec_enum
									 ("connection-style", _("Connection Style"),
									  _("What style of connection this will be, "
										"used to determine proxy information."),
									  GTCP_TYPE_CONNECTION_STYLE,
									  GTCP_CONNECTION_OTHER,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_USE_SSL,
									 g_param_spec_boolean
									 ("use-ssl", _("Use SSL"),
									  _("Whether or not to use SSL in this connection."),
									  FALSE, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_BUFFER_SIZE,
									 g_param_spec_uint
									 ("buffer-size", _("Buffer Size"),
									  _("The max incoming buffer size."),
									  0, G_MAXUINT,
									  GTCP_SOCKET_DEFAULT_BUFFER_SIZE,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_IP_ADDRESS,
									 g_param_spec_string
									 ("ip-address", _("IP Address"),
									  _("The IP address connected to."), NULL,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_STATUS,
									 g_param_spec_enum
									 ("status", _("Connection Status"),
									  _("The current status of the connection."),
									  GTCP_TYPE_CONNECTION_STATUS,
									  GTCP_CONNECTION_CLOSED,
									  (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
	g_object_class_install_property (object_class, PROP_IS_SERVER,
									 g_param_spec_boolean
									 ("is-server", _("Connection is a Server"),
									  _("Whether the connection was created from "
										"a server or not. For internal use by GTcpServer "
										"only."),
									  FALSE, (G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (object_class, PROP_BYTES_READ,
									 g_param_spec_ulong
									 ("bytes-read", _("Bytes Read"),
									  _("The total bytes read."),
									  0, G_MAXULONG, 0, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_BYTES_WRITTEN,
									 g_param_spec_ulong
									 ("bytes-written", _("Bytes Written"),
									  _("The total bytes written."),
									  0, G_MAXULONG, 0, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_SOCKET_FD,
									 g_param_spec_int
									 ("socket-fd", _("Socket File Descriptor"),
									  _("The socket file descriptor. For "
										"internal use by GTcpServer only."),
									  -1, G_MAXINT, -1, G_PARAM_WRITABLE));


	gtcp_connection_signals[LOOKUP_DONE] =
		g_signal_new ("lookup-done",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GTcpConnectionClass,
									   lookup_done), NULL, NULL,
					  g_cclosure_marshal_VOID__ENUM, G_TYPE_NONE, 1,
					  GTCP_TYPE_LOOKUP_STATUS);
	gtcp_connection_signals[CONNECT_DONE] =
		g_signal_new ("connect-done",
					  G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GTcpConnectionClass,
									   connect_done), NULL, NULL,
					  g_cclosure_marshal_VOID__ENUM, G_TYPE_NONE, 1,
					  GTCP_TYPE_CONNECTION_STATUS);
	gtcp_connection_signals[RECV] =
		g_signal_new ("recv", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GTcpConnectionClass, recv),
					  NULL, NULL,
					  _gtcp_socket_marshal_VOID__POINTER_UINT,
					  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_UINT);
	gtcp_connection_signals[SEND] =
		g_signal_new ("send", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GTcpConnectionClass, send),
					  NULL, NULL,
					  _gtcp_socket_marshal_VOID__POINTER_UINT,
					  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_UINT);
	gtcp_connection_signals[CLOSED] =
		g_signal_new ("closed", G_TYPE_FROM_CLASS (object_class),
					  G_SIGNAL_RUN_FIRST,
					  G_STRUCT_OFFSET (GTcpConnectionClass, closed),
					  NULL, NULL, g_cclosure_marshal_VOID__BOOLEAN,
					  G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}


static void
gtcp_connection_instance_init (GTcpConnection * conn)
{
	conn->_priv = g_new0 (GTcpConnectionPrivate, 1);

	conn->_priv->address = NULL;

	conn->_priv->status = GTCP_CONNECTION_CLOSED;
	conn->_priv->ip_address = NULL;

	conn->_priv->proxy = NULL;
	conn->_priv->proxy_entry = NULL;
	conn->_priv->entry = NULL;

	conn->_priv->dns_handle = GTCP_DNS_INVALID_HANDLE;

	conn->_priv->io_context = NULL;
	conn->_priv->io_mainloop = NULL;
	conn->_priv->source = NULL;

	conn->_priv->write_queue = NULL;
	conn->_priv->signal_queue = NULL;

	conn->_priv->sockfd = -1;

	conn->_priv->signal_id = -1;
}


/* ************ *
 *  PUBLIC API  *
 * ************ */

GType
gtcp_connection_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GTcpConnectionClass),
			(GBaseInitFunc) gtcp_connection_base_init,
			(GBaseFinalizeFunc) gtcp_connection_base_finalize,
			(GClassInitFunc) gtcp_connection_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,				/* class data */
			sizeof (GTcpConnection),
			0,					/* number of pre-allocs */
			(GInstanceInitFunc) gtcp_connection_instance_init,
			NULL				/* value table */
		};

		object_type = g_type_register_static (G_TYPE_OBJECT, "GTcpConnection",
											  &type_info, 0);
	}

	return object_type;
}


/**
 * gtcp_connection_new:
 * @address: the remote address to connect to.
 * @port: the remote port to connect to.
 * @conn_style: the #GTcpConnectionStyle for this connection.
 * @use_ssl: whether to use SSL or not.
 *
 * Creates a new #GTcpConnection ready to connect to address:port. The @type
 * argument is used to determine what proxy to connect through, so it must be
 * set correctly. The @use_ssl argument is dependent on GTcpSocket being compiled
 * with SSL support.
 *
 * Returns: a new #GTcpConnection-struct.
 *
 * Since: 1.0
 **/
GObject *
gtcp_connection_new (const gchar * address,
					 guint port,
					 GTcpConnectionStyle conn_style,
					 gboolean use_ssl)
{
	g_return_val_if_fail (address != NULL, NULL);
	g_return_val_if_fail (address[0] != '\0', NULL);

	return g_object_new (GTCP_TYPE_CONNECTION,
						 "address", address,
						 "port", port,
						 "connection-style", conn_style, "use-ssl", use_ssl, NULL);
}


/**
 * gtcp_connection_open:
 * @conn: the #GTcpConnection-struct to open.
 *
 * This starts the process to open an already-created #GTcpConnection-struct object.
 * You should connect all the event signals you want to listen for and set the
 * buffer size (if you want to use a buffer size other than
 * #GTCP_SOCKET_DEFAULT_BUFFER_SIZE) before calling this function.
 *
 * Since: 1.0
 **/
void
gtcp_connection_open (GTcpConnection * conn)
{
	GTcpProxyType proxy_type;
	struct in_addr addr;
	GTcpDNSEntry *entry = NULL;

	g_return_if_fail (conn != NULL);
	g_return_if_fail (GTCP_IS_CONNECTION (conn));
	g_return_if_fail (conn->_priv->status < GTCP_CONNECTION_CONNECTED);

	if (conn->_priv->conn_style == GTCP_PROXY_HTTP)
		proxy_type = GTCP_PROXY_HTTP;
	else if (conn->_priv->use_ssl)
		proxy_type = GTCP_PROXY_SSL;
	else
		proxy_type = conn->_priv->conn_style;

	conn->_priv->proxy = (GTcpProxy *) _gtcp_proxy_get_proxy (proxy_type,
															  conn->_priv->address);

	conn->_priv->bytes_read = 0;
	conn->_priv->bytes_written = 0;
	conn->_priv->status = GTCP_CONNECTION_CONNECTING;

	g_object_freeze_notify (G_OBJECT (conn));
	g_object_notify (G_OBJECT (conn), "bytes-read");
	g_object_notify (G_OBJECT (conn), "bytes-written");
	g_object_notify (G_OBJECT (conn), "status");
	g_object_thaw_notify (G_OBJECT (conn));

	if (conn->_priv->proxy != NULL)
	{
		g_mutex_lock (conn->_priv->proxy->mutex);
		if (inet_aton (conn->_priv->proxy->host, &addr))
		{
			entry = g_new0 (GTcpDNSEntry, 1);
			g_assert (entry != NULL);

			entry->status = GTCP_LOOKUP_OK;
			entry->hostname = NULL;
			entry->aliases = NULL;
			entry->ip_addresses = g_slist_append (entry->ip_addresses,
												  conn->_priv->proxy->host);

			entry->status = GTCP_LOOKUP_OK;
			entry->hostname = NULL;
			proxy_dns_callback (entry, conn);
		}
		else
		{
			conn->_priv->dns_handle = gtcp_dns_get (conn->_priv->proxy->host,
													proxy_dns_callback, conn);
		}
		g_mutex_unlock (conn->_priv->proxy->mutex);
	}
	else
	{
		if (inet_aton (conn->_priv->address, &addr))
		{
			entry = g_new0 (GTcpDNSEntry, 1);
			g_assert (entry != NULL);

			entry->status = GTCP_LOOKUP_OK;
			entry->hostname = NULL;
			entry->aliases = NULL;
			entry->ip_addresses = g_slist_append (entry->ip_addresses,
												  conn->_priv->address);
			dns_callback (entry, conn);
		}
		else
		{
			conn->_priv->dns_handle = gtcp_dns_get (conn->_priv->address,
													dns_callback, conn);
		}
	}
}


/**
 * gtcp_connection_close:
 * @conn: the #GTcpConnection-struct to close.
 *
 * This closes a #GTcpConnection-struct if it is open, or stops the connection
 * process if it's already running. Nothing happens if the connection is
 * already closed.
 *
 * Since: 1.0
 **/
void
gtcp_connection_close (GTcpConnection * conn)
{
	GTcpSignal *signal;

	g_return_if_fail (conn != NULL);
	g_return_if_fail (GTCP_IS_CONNECTION (conn));

	if (conn->_priv->status < GTCP_CONNECTION_CONNECTED)
		return;

	signal = g_new0 (GTcpSignal, 1);
	g_assert (signal != NULL);

	signal->id = gtcp_connection_signals[CLOSED];
	signal->arg1 = GINT_TO_POINTER (TRUE);
	signal->arg2 = NULL;

	g_async_queue_push (conn->_priv->signal_queue, signal);
}


/**
 * gtcp_connection_send:
 * @conn: the #GTcpConnection-struct to send data through.
 * @data: the data to send. #GTcpConnection-struct copies this data,
 * so the programmer should free it after it is no longer needed.
 * @length: the number of bytes in data, or %-1 if data is null-terminated.
 * NOTE: If length is greater than the current buffer size (or if it is
 * determined to be greater if %-1 is passed), the data will be split into
 * buffer-sized blocks before sending.
 *
 * This sends length bytes of data over an open #GTcpConnection-struct. Do not pass %-1
 * for the length if data is not nul-terminated, this will cause a crash.
 * NOTE: This function will only return a value other than %GTCP_SEND_DATA_QUEUED
 * if it doesn't know of a reason to not send the data or queue for sending
 * later. When the data is actually sent, the "send" signal will be emitted.
 * 
 * Returns: #GTcpSendStatus indicating how the data was handled.
 *
 * Since: 1.0
 **/
GTcpSendStatus
gtcp_connection_send (GTcpConnection * conn,
					  gconstpointer data,
					  gssize length)
{
	gpointer real_data = NULL;

	g_return_val_if_fail (conn != NULL, GTCP_SEND_ERROR);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), GTCP_SEND_ERROR);
	g_return_val_if_fail (conn->_priv->status == GTCP_CONNECTION_CONNECTED,
						  GTCP_SEND_ERROR_NOT_OPEN);
	g_return_val_if_fail (data != NULL, GTCP_SEND_ERROR_NULL_DATA);

	if (length < 0)
	{
		for (length = 0; *((guchar *) data + length) != 0; length++);
	}

	real_data = g_memdup (data, length);

	return append_data_to_queue (conn, real_data, length);
}


/**
 * gtcp_connection_set_conn_style:
 * @conn: the closed #GTcpConnection-struct to change the connection type of.
 * @conn_style: the new connection style.
 *
 * This changes the connection type of closed #GTcpConnection-struct. The connection
 * type is used (in conjunction with the SSL option and the destination
 * address) to determine which proxy to use, if any.
 *
 * Since: 1.0
 **/
void
gtcp_connection_set_conn_style (GTcpConnection * conn,
								GTcpConnectionStyle conn_style)
{
	g_return_if_fail (conn != NULL);
	g_return_if_fail (GTCP_IS_CONNECTION (conn));
	g_return_if_fail (conn->_priv->status < GTCP_CONNECTION_CONNECTED);

	conn->_priv->conn_style = conn_style;
}


/**
 * gtcp_connection_get_conn_style:
 * @conn: the #GTcpConnection-struct to retrieve the connection type of.
 *
 * This retrieves the connection type of a closed #GTcpConnection. The
 * connection type is used (in conjunction with the SSL option and the
 * destination address) to determine which proxy to use, if any.
 * 
 * Returns: the current #GTcpConnectionStyle.
 *
 * Since: 1.0
 **/
GTcpConnectionStyle
gtcp_connection_get_conn_style (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, -1);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), -1);

	return conn->_priv->conn_style;
}


/**
 * gtcp_connection_set_address:
 * @conn: the closed #GTcpConnection-struct to change the address of.
 * @address: the new address.
 *
 * This changes the destination of a closed #GTcpConnection-struct.
 *
 * Since: 1.0
 **/
void
gtcp_connection_set_address (GTcpConnection * conn,
							 const gchar * address)
{
	g_return_if_fail (conn != NULL);
	g_return_if_fail (GTCP_IS_CONNECTION (conn));
	g_return_if_fail (conn->_priv->status < GTCP_CONNECTION_CONNECTED);

	g_return_if_fail (address != NULL);
	g_return_if_fail (address[0] != '\0');

	if (conn->_priv->address)
		g_free (conn->_priv->address);

	conn->_priv->address = g_strdup (address);
}


/**
 * gtcp_connection_get_address:
 * @conn: the #GTcpConnection-struct to retrieve the address of
 *
 * This gets the destination of a #GTcpConnection-struct. This value should
 * not be modified.
 * 
 * Returns: The destination IP address or hostname string.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gtcp_connection_get_address (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, NULL);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), NULL);

	return conn->_priv->address;
}


/**
 * gtcp_connection_get_ip_address:
 * @conn: the #GTcpConnection-struct to retrieve the address of.
 *
 * This gets the destination IP of a #GTcpConnection-struct. This value should
 * not be modified, and may be NULL if the address could not resolve to
 * an IP address, or the connection is closed.
 * 
 * Returns: the destination IP address.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gtcp_connection_get_ip_address (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, NULL);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), NULL);

	return conn->_priv->ip_address;
}


/**
 * gtcp_connection_set_port:
 * @conn: the closed #GTcpConnection-struct to change the destination port of.
 * @port: the new port.
 *
 * This sets the destination port of a closed #GTcpConnection-struct.
 *
 * Since: 1.0
 **/
void
gtcp_connection_set_port (GTcpConnection * conn,
						  guint port)
{
	g_return_if_fail (conn != NULL);
	g_return_if_fail (GTCP_IS_CONNECTION (conn));
	g_return_if_fail (conn->_priv->status < GTCP_CONNECTION_CONNECTED);

	conn->_priv->port = port;
}


/**
 * gtcp_connection_get_port:
 * @conn: the #GTcpConnection-struct to get the destination port of.
 *
 * This retrieves the destination port of a #GTcpConnection-struct.
 * 
 * Returns: the destination port.
 *
 * Since: 1.0
 **/
guint
gtcp_connection_get_port (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, 0);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), 0);

	return conn->_priv->port;
}


/**
 * gtcp_connection_set_use_ssl:
 * @conn: the closed #GTcpConnection-struct to modify.
 * @use_ssl: whether or not to use ssl.
 *
 * This sets whether or not to encrypt connections over this closed
 * #GTcpConnection-struct. It will return %TRUE if SSL is supported, or
 * %FALSE, if it is not.
 * 
 * Returns: whether or not SSL is supported.
 *
 * Since: 1.0
 **/
gboolean
gtcp_connection_set_use_ssl (GTcpConnection * conn,
							 gboolean use_ssl)
{
	g_return_val_if_fail (conn != NULL, FALSE);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), FALSE);
	g_return_val_if_fail (conn->_priv->status < GTCP_CONNECTION_CONNECTED, FALSE);

#ifdef USE_SSL
	conn->_priv->use_ssl = use_ssl;
	return TRUE;
#else
	return FALSE;
#endif
}


/**
 * gtcp_connection_get_use_ssl:
 * @conn: the #GTcpConnection-struct to modify.
 *
 * This retrieves whether or not SSL will be used in this #GTcpConnection-struct.
 * 
 * Returns: whether or not SSL will be used in this #GTcpConnection.
 *
 * Since: 1.0
 **/
gboolean
gtcp_connection_get_use_ssl (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, FALSE);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), FALSE);

	return conn->_priv->use_ssl;
}


/**
 * gtcp_connection_set_buffer_size:
 * @conn: the #GTcpConnection-struct to change the buffer size of.
 * @buffer_size: the new buffer size.
 *
 * This sets the buffer size of a closed #GTcpConnection-struct.
 * See also: #GTCP_SOCKET_DEFAULT_BUFFER_SIZE.
 *
 * Since: 1.0
 **/
void
gtcp_connection_set_buffer_size (GTcpConnection * conn,
								 gsize buffer_size)
{
	g_return_if_fail (conn != NULL);
	g_return_if_fail (GTCP_IS_CONNECTION (conn));
	g_return_if_fail (conn->_priv->status < GTCP_CONNECTION_CONNECTED);

	conn->_priv->buffer_size = buffer_size;
}


/**
 * gtcp_connection_get_buffer_size:
 * @conn: the #GTcpConnection-struct to retrieve the buffer size of.
 *
 * This retrieves the buffer size of a closed #GTcpConnection-struct.
 * See also: #GTCP_SOCKET_DEFAULT_BUFFER_SIZE.
 * 
 * Returns: the current buffer size.
 *
 * Since: 1.0
 **/
gsize
gtcp_connection_get_buffer_size (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, 0);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), 0);

	return conn->_priv->buffer_size;
}


/**
 * gtcp_connection_get_bytes_read:
 * @conn: the #GTcpConnection-struct to get the number of bytes read from.
 *
 * This gets the number of bytes recieved by a #GTcpConnection-struct for this
 * connection. This counter is reset to zero when the connection is opened,
 * so if you close the #GTcpConnection-struct, then open it, the counter will be
 * reset.
 * 
 * Returns: the number of bytes read so far.
 *
 * Since: 1.0
 **/
gulong
gtcp_connection_get_bytes_read (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, 0);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), 0);

	return conn->_priv->bytes_read;
}


/**
 * gtcp_connection_get_bytes_written:
 * @conn: the #GTcpConnection-struct to get the number of bytes written to.
 *
 * This gets the number of bytes recieved by a #GTcpConnection-struct for this
 * connection. This counter is reset to zero when the connection is opened,
 * so if you close the #GTcpConnection-struct, then open it, the counter will be
 * reset.
 * 
 * Returns: the number of bytes sent so far.
 *
 * Since: 1.0
 **/
gulong
gtcp_connection_get_bytes_written (GTcpConnection * conn)
{
	g_return_val_if_fail (conn != NULL, 0);
	g_return_val_if_fail (GTCP_IS_CONNECTION (conn), 0);

	return conn->_priv->bytes_written;
}
