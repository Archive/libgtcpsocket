/*
 * LibGTcpSocket: libgtcpsocket/gtcp-socket.h
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_SOCKET_H__
#define __GTCP_SOCKET_H__


#include "gtcp-connection.h"
#include "gtcp-dns.h"
#include "gtcp-i18n.h"
#include "gtcp-proxy.h"
#include "gtcp-server.h"
#include "gtcp-socket-types.h"
#include "gtcp-socket-type-builtins.h"


#ifndef GTCP_SOCKET_DEFAULT_BUFFER_SIZE
#	define GTCP_SOCKET_DEFAULT_BUFFER_SIZE 2048
#endif /* GTCP_SOCKET_DEFAULT_BUFFER_SIZE */


#endif /* __GTCP_SOCKET_H__ */
