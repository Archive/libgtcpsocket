/* 
 * LibGTcpSocket: libgtcpsocket/gtcp-connection.h
 *
 * Copyright (C) 2001-2002 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_CONNECTION_H__
#define __GTCP_CONNECTION_H__ 1

#include <glib.h>
#include <glib-object.h>

#include "gtcp-socket-types.h"


#define GTCP_TYPE_CONNECTION			(gtcp_connection_get_type ())
#define GTCP_CONNECTION(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTCP_TYPE_CONNECTION, GTcpConnection))
#define GTCP_CONNECTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GTCP_TYPE_CONNECTION, GTcpConnectionClass))
#define GTCP_IS_CONNECTION(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTCP_TYPE_CONNECTION))
#define GTCP_IS_CONNECTION_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GTCP_TYPE_CONNECTION))
#define GTCP_CONNECTION_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GTCP_TYPE_CONNECTION, GTcpConnectionClass))


G_BEGIN_DECLS


typedef struct _GTcpConnection GTcpConnection;
typedef struct _GTcpConnectionClass GTcpConnectionClass;
typedef struct _GTcpConnectionPrivate GTcpConnectionPrivate;


struct _GTcpConnection
{
	/* < private > */
	GObject parent;

	GTcpConnectionPrivate *_priv;
};

struct _GTcpConnectionClass
{
	/* <private > */
	GObjectClass parent_class;

	/* < public > */
	/* Signals */
	void (*lookup_done)   (GTcpConnection * conn,
	                       GTcpLookupStatus status);
	void (*connect_done)  (GTcpConnection * conn,
	                       GTcpConnectionStatus status);
	void (*recv)          (GTcpConnection * conn,
	                       gconstpointer data,
	                       gsize length);
	void (*send)          (GTcpConnection * conn,
	                       gconstpointer data,
	                       gsize length);
	void (*closed)        (GTcpConnection * conn,
	                       gboolean requested);

	/* < private > */
	void (*__gtcp_reserved_1);
	void (*__gtcp_reserved_2);
	void (*__gtcp_reserved_3);
	void (*__gtcp_reserved_4);
};


GType gtcp_connection_get_type (void);

GObject *gtcp_connection_new (const gchar * address,
							  guint port,
							  GTcpConnectionStyle conn_style,
							  gboolean use_ssl);

void gtcp_connection_open (GTcpConnection * conn);
void gtcp_connection_close (GTcpConnection * conn);
GTcpSendStatus gtcp_connection_send (GTcpConnection * conn,
									 gconstpointer data,
									 gssize length);

/* Only call these on closed objects */
void gtcp_connection_set_conn_style (GTcpConnection * conn,
									 GTcpConnectionStyle conn_style);
GTcpConnectionStyle gtcp_connection_get_conn_style (GTcpConnection * conn);

void gtcp_connection_set_address (GTcpConnection * conn,
								  const gchar * address);
G_CONST_RETURN gchar *gtcp_connection_get_address (GTcpConnection * conn);

G_CONST_RETURN gchar *gtcp_connection_get_ip_address (GTcpConnection * conn);

void gtcp_connection_set_port (GTcpConnection * conn,
							   guint port);
guint gtcp_connection_get_port (GTcpConnection * conn);

/* Returns whether or not it actually will use SSL, SSL must be compiled
   into GTcpSocket for this to ever return TRUE. */
gboolean gtcp_connection_set_use_ssl (GTcpConnection * conn,
									  gboolean use_ssl);
gboolean gtcp_connection_get_use_ssl (GTcpConnection * conn);

void gtcp_connection_set_buffer_size (GTcpConnection * conn,
									  gsize buffer_size);
gsize gtcp_connection_get_buffer_size (GTcpConnection * conn);


/* Doesn't matter when these are called -- Note: the bytes written/read are
   reset when gtcp_connection_open() is called */
gulong gtcp_connection_get_bytes_read (GTcpConnection * conn);
gulong gtcp_connection_get_bytes_written (GTcpConnection * conn);


G_END_DECLS

#endif /* __GTCP_CONNECTION_H__ */
