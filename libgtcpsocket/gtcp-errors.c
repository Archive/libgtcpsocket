/* 
 * LibGTcpSocket: libgtcpsocket/gtcp-errors.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

Get GErrors/error strings for various status messages.

Notes on editing:
	Tab size: 4
*/


#include "gtcp-errors.h"
#include "gtcp-i18n.h"


typedef struct
{
	gint id;
	gchar *str;
}
GTcpEnumString;


static const GTcpEnumString lookup_msgs[] = {
	{GTCP_LOOKUP_IN_PROGRESS,
	 N_("Search for \"%s\" in progress...")},
	{GTCP_LOOKUP_OK,
	 N_("%s found")},
	{GTCP_LOOKUP_ERROR_NOT_FOUND,
	 N_("\"%s\" could not be found. The name might be misspelled, or it may "
		"not exist.")},
	{GTCP_LOOKUP_ERROR_NO_RECOVERY,
	 N_("The DNS lookup server could not be contacted to find %s. The network "
		"may be down, or the DNS server may be broken.")},
	{GTCP_LOOKUP_ERROR_TRY_AGAIN,
	 N_("The DNS lookup server is temporarily unavailable, try connecting in a "
		"few minutes.")},
	{GTCP_LOOKUP_ERROR_IPV4_IPV6_MISMATCH,
	 N_("The network is misconfigured, and \"%s\" cannot be connected to. "
		"Contact your network administrator or ISP for more information.")},
	{GTCP_LOOKUP_ERROR_THREAD_ERROR,
	 N_("Your computer is unable to use a real application, install a threads "
		"implementation or upgrade. :-P")},
	{0, NULL}
};


static const GTcpEnumString conn_msgs[] = {
	{GTCP_CONNECTION_ERROR_CONNECTION_REFUSED,
	 N_("%s has will not allow you to connect to the requested service. The "
		"port number may be incorrect.")},
	{GTCP_CONNECTION_ERROR_TIMEOUT,
	 N_("The connection attempt took too long to complete. %s may be down, or "
		"you may have been disconnected from the network.")},
	{GTCP_CONNECTION_ERROR_NETWORK_UNREACHABLE,
	 N_("You cannot connect to %s, because it is on a network you cannot "
		"connect to. The address may be incorrect, or you may have been "
		"disconnected.")},
	{GTCP_CONNECTION_ERROR_BAD_BROADCAST_OR_FIREWALL,
	 N_("You cannot connect to %s, because your computer or firewall is "
		"configured to prevent it.")},
	{GTCP_CONNECTION_ERROR_THREAD_ERROR,
	 N_("You cannot connect to %s, because your computer does not support "
		"threaded applications.")},
	{GTCP_CONNECTION_ERROR_PROXY_ERROR,
	 N_("You cannot connect to %s, because your proxy server could not be "
		"found. Your computer may be misconfigured, or the proxy server may "
		"be down.")},
	{0, NULL}
};


static const GTcpEnumString servopen_msgs[] = {
	{GTCP_SERVER_OPEN_OK,
	 N_("Your computer is now accepting connections to port %u.")},
	{GTCP_SERVER_OPEN_OPENING,
	 N_("Your computer is preparing to accept connections to port %u.")},
	{GTCP_SERVER_OPEN_ERROR_NO_LOCAL_HOSTNAME,
	 N_("You cannot accept connections on port %u, because your computer "
		"does not have a local address.")},
	{GTCP_SERVER_OPEN_ERROR_SOCKET_TYPE_PERMS,
	 N_("You cannot accept connections on port %u, because you do not have "
		"permission to use TCP/IP connections.")},
	{GTCP_SERVER_OPEN_ERROR_NO_MEM,
	 N_("You cannot accept connections on port %u, because your computer "
		"is out of memory for networking purposes.")},
	{GTCP_SERVER_OPEN_ERROR_TABLE_OVERFLOW,
	 N_("You cannot accept connections on port %u, because your computer "
		"has too many running applications.")},
	{GTCP_SERVER_OPEN_ERROR_ALREADY_BOUND,
	 N_("You cannot accept connections on port %u, because that port is "
		"already being used.")},
	{GTCP_SERVER_OPEN_ERROR_LOW_PORT_PERMS,
	 N_("You cannot accept connections on port %u, because you do not have "
		"permission to accept connections on ports lower than 1024.")},
	{GTCP_SERVER_OPEN_ERROR_INTERNAL_ERROR,
	 N_("You cannot accept connections on port %u, because an error "
		"occurred inside the networking library.")},
	{0, NULL}
};


/**
 * gtcp_error_get_lookup_status_message:
 * @status: the #GTcpLookupStatus for this connection.
 * @address: the remote address attempted.
 *
 * Allocates a properly formatted error message for the DNS lookup status,
 * suitable for use in an error message or dialog. The returned string should
 * be freed when it is no longer needed.
 *
 * Returns: an error string for @status.
 *
 * Since: 1.0
 **/
gchar *
gtcp_error_get_lookup_status_message (GTcpLookupStatus status,
									  const gchar * address)
{
	guint i;

	for (i = 0;
		 i < G_N_ELEMENTS (lookup_msgs) && lookup_msgs[i].str != NULL
		 && lookup_msgs[i].id != status; i++);

	return (lookup_msgs[i].str != NULL ?
			g_strdup_printf (lookup_msgs[i].str, address) :
			N_("This application is doing some strange foo."));
}


/**
 * gtcp_error_get_connection_status_message:
 * @status: the #GTcpConnectionStatus for this connection.
 * @address: the remote address attempted.
 * @port: the remote port attempted.
 *
 * Allocates a properly formatted error message for the connection status,
 * suitable for use in an error message or dialog. The returned string should
 * be freed when it is no longer needed.
 *
 * Returns: an error string for @status.
 *
 * Since: 1.0
 **/
gchar *
gtcp_error_get_connection_status_message (GTcpConnectionStatus status,
										  const gchar * address,
										  guint port)
{
	guint i;

	for (i = 0;
		 i < G_N_ELEMENTS (conn_msgs) && conn_msgs[i].str != NULL
		 && conn_msgs[i].id != (gint) status; i++);

	return (conn_msgs[i].str != NULL ?
			g_strdup_printf (conn_msgs[i].str, address, port) :
			N_("This application is doing some strange foo."));
}


/**
 * gtcp_error_get_server_open_status_message:
 * @status: the #GTcpServerOpenStatus for this server.
 * @port: the local port attempted.
 *
 * Allocates a properly formatted error message for the open status,
 * suitable for use in an error message or dialog. The returned string should
 * be freed when it is no longer needed.
 *
 * Returns: an error string for @status.
 *
 * Since: 1.0
 **/
gchar *
gtcp_error_get_server_open_status_message (GTcpServerOpenStatus status,
										   guint port)
{
	guint i;

	for (i = 0;
		 i < G_N_ELEMENTS (servopen_msgs) && servopen_msgs[i].str != NULL
		 && servopen_msgs[i].id != (gint) status; i++);

	return (servopen_msgs[i].str != NULL ?
			g_strdup_printf (servopen_msgs[i].str, port) :
			N_("This application is doing some strange foo."));
}
