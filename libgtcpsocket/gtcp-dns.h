/* LibGTcpSocket: src/gtcp-dns.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0 DNS lookup backend.

Notes on editing:
	Tab size: 4
*/


#ifndef __GTCP_DNS_H__
#define __GTCP_DNS_H__ 1

#include <glib.h>
#include <glib-object.h>

#include "gtcp-socket-types.h"

G_BEGIN_DECLS

#define GTCP_TYPE_DNS_ENTRY		(gtcp_dns_entry_get_type ())
#define GTCP_DNS_ENTRY(ptr)		((GTcpDNSEntry *)(ptr))
#define GTCP_DNS_INVALID_HANDLE	-1


typedef glong GTcpDNSHandle;
typedef struct _GTcpDNSEntry GTcpDNSEntry;

/**
 * GTcpDNSCallbackFunc:
 * @entry: the DNS lookup return.
 * @user_data: the specified user data.
 * 
 * The callback function invoked when a DNS lookup has returned.
 * Note that @entry is not gauranteed to be static for any longer
 * than the duration of the callback function. If you wish to
 * save the data in @entry, you should copy it with gtcp_dns_entry_copy().
 *
 * Since: 1.0 
 **/
typedef void (*GTcpDNSCallbackFunc) (const GTcpDNSEntry * entry,
									 gpointer user_data);


/**
 * GTcpDNSEntry:
 * @status: the status code of the lookup (see #GTcpLookupStatus and gtcp_error_get_lookup_status_message()).
 * @hostname: the cannonical hostname (may be NULL if there is no DNS record for this hostname or address).
 * @aliases: a list of hostname aliases for this DNS record (if any).
 * @ip_addresses: a list of IP addresses for this DNS record (if any).
 * 
 * An enhanced version of the standard hostent struct.
 *
 * Since: 1.0
 **/
struct _GTcpDNSEntry
{
	GTcpLookupStatus status;

	gchar *hostname;
	GSList *aliases;
	GSList *ip_addresses;
};

/**
 * gtcp_dns_get:
 * @address: the hostname or IP address to lookup.
 * @callback: the callback function which will be called when the DNS lookup has finished.
 * @user_data: the user data to pass the callback function.
 *
 * Performs a DNS lookup of @address.
 * 
 * Returns: a new #GTcpDNSHandle
 * 
 * Since: 1.0
 **/
#define gtcp_dns_get(address,callback,user_data) (gtcp_dns_get_full (address, \
																	 callback, \
																	 user_data, \
																	 NULL))
GTcpDNSHandle gtcp_dns_get_full (const gchar *address,
								 GTcpDNSCallbackFunc callback,
								 gpointer user_data,
								 GDestroyNotify destroy_data);
void gtcp_dns_cancel (GTcpDNSHandle handle);

void gtcp_dns_entry_free (GTcpDNSEntry * entry);
GTcpDNSEntry *gtcp_dns_entry_copy (const GTcpDNSEntry *src);

GType gtcp_dns_entry_get_type (void);


G_END_DECLS 
#endif /* __GTCP_DNS_H__ */
