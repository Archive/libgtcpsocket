/* 
 * LibGTcpSocket: libgtcpsocket/gtcp-proxy.c
 *
 * Copyright (C) 2001 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*

GLib 2.0/GObject-based TCP/IP networking sockets wrapper.

Notes on editing:
	Tab size: 4
*/


#include "gtcp-proxy.h"
#include "gtcp-proxy-private.h"
#include "proxy-keys.h"

#include <gconf/gconf-client.h>

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


enum
{
	HTTP_PROXY,
	FTP_PROXY,
	SSL_PROXY,
	SOCKS_PROXY,
	LAST_ITEM
};


typedef enum
{
	GTCP_PROXY_DIRECT,
	GTCP_PROXY_MANUAL,
	GTCP_PROXY_AUTO
}
GTcpProxyMode;


typedef struct
{
	GConfClient *client;

	gint notify_ids[2];
	GTcpProxy *proxies[LAST_ITEM];

	GSList *exceptions;
	GTcpProxyMode mode:2;
}
GTcpProxySettings;


/* Socks Version 4 */
typedef struct
{
	guint8 version;
	guint8 command;

	guint16 port;
	guint32 address;
}
GTcpSocks4Data;


/* Socks Version 5 */
typedef struct
{
	guint8 version;
	guint8 num_methods;
	guint8 methods;
}
GTcpSocks5NegotiationData;

typedef struct
{
	guint8 version;
	guint8 command;
	guint8 reserved;
	guint8 address_type;

	guint32 address;
	guint16 port;
}
GTcpSocks5RequestData;

#define SOCKS5_COMMAND_CONNECT 0x01
#define SOCKS5_COMMAND_BIND 0x02

#define SOCKS5_TYPE_IPV4 0x01
#define SOCKS5_TYPE_DOMAINNAME 0x03
#define SOCKS5_TYPE_IPV6 0x04


static gint num_clients = 0;
static GTcpProxySettings *settings = NULL;

static const GConfEnumStringPair mode_lookup_table[] = {
	{GTCP_PROXY_DIRECT, "direct"},
	{GTCP_PROXY_MANUAL, "manual"},
	{GTCP_PROXY_AUTO, "auto"},
	{0, NULL}
};


/* ********************************** *
 *  LOCAL FUNCTIONS                   *
 * ********************************** */

static void
http_proxy_notify (GConfClient * client,
				   guint cnxn_id,
				   GConfEntry * entry,
				   gpointer data)
{
	g_mutex_lock (settings->proxies[HTTP_PROXY]->mutex);
	if (strcmp (entry->key, HTTP_PROXY_HOST_KEY) == 0)
	{
		if (settings->proxies[HTTP_PROXY]->host != NULL)
			g_free (settings->proxies[HTTP_PROXY]->host);

		settings->proxies[HTTP_PROXY]->host =
			g_strdup (gconf_value_get_string (entry->value));

		settings->proxies[HTTP_PROXY]->use_proxy =
			(settings->proxies[HTTP_PROXY]->host != NULL);
	}
	else if (strcmp (entry->key, HTTP_PROXY_PORT_KEY) == 0)
	{
		settings->proxies[HTTP_PROXY]->port = gconf_value_get_int (entry->value);
	}
	else if (strcmp (entry->key, HTTP_PROXY_USE_AUTH_KEY) == 0)
	{
		settings->proxies[HTTP_PROXY]->use_auth = gconf_value_get_bool (entry->value);
	}
	else if (strcmp (entry->key, HTTP_PROXY_AUTH_USER_KEY) == 0)
	{
		if (settings->proxies[HTTP_PROXY]->username != NULL)
			g_free (settings->proxies[HTTP_PROXY]->username);

		settings->proxies[HTTP_PROXY]->username =
			g_strdup (gconf_value_get_string (entry->value));
	}
	else if (strcmp (entry->key, HTTP_PROXY_AUTH_PASS_KEY) == 0)
	{
		if (settings->proxies[HTTP_PROXY]->passwd != NULL)
			g_free (settings->proxies[HTTP_PROXY]->passwd);

		settings->proxies[HTTP_PROXY]->passwd =
			g_strdup (gconf_value_get_string (entry->value));
	}

	g_mutex_unlock (settings->proxies[HTTP_PROXY]->mutex);
}


static void
proxy_notify (GConfClient * client,
			  guint cnxn_id,
			  GConfEntry * entry,
			  gpointer data)
{
	/* Global */
	if (strcmp (entry->key, PROXY_MODE_KEY) == 0)
	{
		gint mode;

		if (gconf_string_to_enum ((GConfEnumStringPair *) mode_lookup_table,
								  gconf_value_get_string (entry->value), &mode))
		{
			settings->mode = mode;
		}
	}
	/* FTP Proxies */
	else if (strcmp (entry->key, FTP_PROXY_HOST_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[FTP_PROXY]->mutex);

		if (settings->proxies[FTP_PROXY]->host != NULL)
			g_free (settings->proxies[FTP_PROXY]->host);

		settings->proxies[FTP_PROXY]->host =
			g_strdup (gconf_value_get_string (entry->value));

		g_mutex_unlock (settings->proxies[FTP_PROXY]->mutex);
	}
	else if (strcmp (entry->key, FTP_PROXY_PORT_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[FTP_PROXY]->mutex);

		settings->proxies[FTP_PROXY]->port = gconf_value_get_int (entry->value);

		g_mutex_unlock (settings->proxies[FTP_PROXY]->mutex);
	}
	/* SSL Proxies */
	else if (strcmp (entry->key, SSL_PROXY_HOST_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[SSL_PROXY]->mutex);

		if (settings->proxies[SSL_PROXY]->host != NULL)
			g_free (settings->proxies[SSL_PROXY]->host);

		settings->proxies[SSL_PROXY]->host =
			g_strdup (gconf_value_get_string (entry->value));
		settings->proxies[SSL_PROXY]->use_proxy =
			(settings->proxies[SSL_PROXY]->host != NULL);

		g_mutex_unlock (settings->proxies[SSL_PROXY]->mutex);
	}
	else if (strcmp (entry->key, SSL_PROXY_PORT_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[SSL_PROXY]->mutex);

		settings->proxies[SSL_PROXY]->port = gconf_value_get_int (entry->value);

		g_mutex_unlock (settings->proxies[SSL_PROXY]->mutex);
	}
	/* SOCKS Proxies */
	else if (strcmp (entry->key, SOCKS_PROXY_HOST_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[SOCKS_PROXY]->mutex);

		if (settings->proxies[SOCKS_PROXY]->host != NULL)
			g_free (settings->proxies[SOCKS_PROXY]->host);

		settings->proxies[SOCKS_PROXY]->host =
			g_strdup (gconf_value_get_string (entry->value));
		settings->proxies[SOCKS_PROXY]->use_proxy =
			(settings->proxies[SOCKS_PROXY]->host != NULL);

		g_mutex_unlock (settings->proxies[SOCKS_PROXY]->mutex);
	}
	else if (strcmp (entry->key, SOCKS_PROXY_PORT_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[SOCKS_PROXY]->mutex);

		settings->proxies[SOCKS_PROXY]->port = gconf_value_get_int (entry->value);

		g_mutex_unlock (settings->proxies[SOCKS_PROXY]->mutex);
	}
	else if (strcmp (entry->key, SOCKS_PROXY_VERSION_KEY) == 0)
	{
		g_mutex_lock (settings->proxies[SOCKS_PROXY]->mutex);

		if (gconf_value_get_bool (entry->value))
		{
			settings->proxies[SOCKS_PROXY]->type = GTCP_PROXY_SOCKS5;
		}
		else
		{
			settings->proxies[SOCKS_PROXY]->type = GTCP_PROXY_SOCKS4;
		}

		g_mutex_unlock (settings->proxies[SOCKS_PROXY]->mutex);
	}
}


static GTcpProxy *
gtcp_proxy_new (GTcpProxyType type)
{
	GTcpProxy *proxy = g_new0 (GTcpProxy, 1);

	g_assert (proxy != NULL);

	proxy->type = type;

	switch (type)
	{
	case GTCP_PROXY_HTTP:
		proxy->use_proxy = gconf_client_get_bool (settings->client,
												  HTTP_PROXY_USE_PROXY_KEY, NULL);
		proxy->host = gconf_client_get_string (settings->client, HTTP_PROXY_HOST_KEY,
											   NULL);
		proxy->port = gconf_client_get_int (settings->client, HTTP_PROXY_PORT_KEY, NULL);
		proxy->use_auth = gconf_client_get_bool (settings->client,
												 HTTP_PROXY_USE_AUTH_KEY, NULL);
		proxy->username = gconf_client_get_string (settings->client,
												   HTTP_PROXY_AUTH_USER_KEY, NULL);
		proxy->passwd = gconf_client_get_string (settings->client,
												 HTTP_PROXY_AUTH_PASS_KEY, NULL);
		break;

	case GTCP_PROXY_FTP:
		proxy->host = gconf_client_get_string (settings->client, FTP_PROXY_HOST_KEY,
											   NULL);
		proxy->use_proxy = (proxy->host != NULL);
		proxy->port = gconf_client_get_int (settings->client, FTP_PROXY_PORT_KEY, NULL);
		break;

	case GTCP_PROXY_SSL:
		proxy->host = gconf_client_get_string (settings->client, SSL_PROXY_HOST_KEY,
											   NULL);
		proxy->use_proxy = (proxy->host != NULL);
		proxy->port = gconf_client_get_int (settings->client, SSL_PROXY_PORT_KEY, NULL);
		break;

	case GTCP_PROXY_OTHER:
	case GTCP_PROXY_SOCKS4:
	case GTCP_PROXY_SOCKS5:
		proxy->host = gconf_client_get_string (settings->client, SOCKS_PROXY_HOST_KEY,
											   NULL);
		proxy->use_proxy = (proxy->host != NULL);
		proxy->port = gconf_client_get_int (settings->client, SOCKS_PROXY_PORT_KEY, NULL);
		if (gconf_client_get_bool (settings->client, SOCKS_PROXY_VERSION_KEY, NULL))
		{
			proxy->type = GTCP_PROXY_SOCKS5;
		}
		else
		{
			proxy->type = GTCP_PROXY_SOCKS4;
		}
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	if (!g_thread_supported ())
		g_thread_init (NULL);

	proxy->mutex = g_mutex_new ();

	return proxy;
}


static void
gtcp_proxy_destroy (GTcpProxy * proxy)
{
	if (proxy != NULL)
	{
		g_free (proxy->host);
		g_free (proxy->username);
		g_free (proxy->passwd);

		g_mutex_free (proxy->mutex);

		g_free (proxy);
	}
}


/* ********************************** *
 *  PRIVATE API                       *
 * ********************************** */


void
_gtcp_proxy_initialize (void)
{
	num_clients++;

	if (settings != NULL)
		return;

	settings = g_new0 (GTcpProxySettings, 1);

	settings->exceptions = NULL;

	settings->client = gconf_client_get_default ();

	gconf_client_add_dir (settings->client, HTTP_PROXY_DIR,
						  GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	gconf_client_add_dir (settings->client, PROXY_DIR,
						  GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);

	/* Load proxies */
	settings->proxies[HTTP_PROXY] = gtcp_proxy_new (GTCP_PROXY_HTTP);
	settings->proxies[FTP_PROXY] = gtcp_proxy_new (GTCP_PROXY_FTP);
	settings->proxies[SSL_PROXY] = gtcp_proxy_new (GTCP_PROXY_SSL);
	settings->proxies[SOCKS_PROXY] = gtcp_proxy_new (GTCP_PROXY_OTHER);

	settings->notify_ids[0] = gconf_client_notify_add (settings->client,
													   HTTP_PROXY_DIR,
													   http_proxy_notify,
													   NULL, NULL, NULL);
	settings->notify_ids[1] = gconf_client_notify_add (settings->client,
													   PROXY_DIR,
													   proxy_notify, NULL, NULL, NULL);
}


void
_gtcp_proxy_shutdown (void)
{
	gint i;

	num_clients--;

	if (num_clients > 0)
		return;

	gconf_client_notify_remove (settings->client, settings->notify_ids[0]);
	gconf_client_notify_remove (settings->client, settings->notify_ids[1]);

	gconf_client_remove_dir (settings->client, HTTP_PROXY_DIR, NULL);
	gconf_client_remove_dir (settings->client, PROXY_DIR, NULL);

	for (i = 0; i < LAST_ITEM; i++)
	{
		gtcp_proxy_destroy (settings->proxies[i]);
	}

	for (; settings->exceptions != NULL;
		 settings->exceptions = g_slist_remove_link (settings->exceptions,
													 settings->exceptions))
	{
		g_free (settings->exceptions->data);
	}

	g_free (settings);
	settings = NULL;
}


G_CONST_RETURN GTcpProxy *
_gtcp_proxy_get_proxy (GTcpProxyType type,
					   const gchar * address)
{
	GSList *exceptions;
	GTcpProxy *proxy = NULL;

	g_return_val_if_fail (address != NULL, NULL);
	g_return_val_if_fail (address[0] != '\0', NULL);

	_gtcp_proxy_initialize ();

	for (exceptions = settings->exceptions; exceptions != NULL;
		 exceptions = exceptions->next)
	{
		if (g_ascii_strcasecmp (address, exceptions->data) == 0)
		{
			return NULL;
		}
	}

	switch (type)
	{
	case GTCP_PROXY_HTTP:
		if (settings->proxies[HTTP_PROXY]->use_proxy
			&& settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[HTTP_PROXY];
		}
		else if (settings->proxies[SOCKS_PROXY]->host != NULL
				 && settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[SOCKS_PROXY];
		}
		break;

	case GTCP_PROXY_FTP:
		if (settings->proxies[FTP_PROXY]->host != NULL
			&& settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[FTP_PROXY];
		}
		else if (settings->proxies[SOCKS_PROXY]->host != NULL
				 && settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[SOCKS_PROXY];
		}
		break;

	case GTCP_PROXY_SSL:
		if (settings->proxies[SSL_PROXY]->host != NULL
			&& settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[SSL_PROXY];
		}
		else if (settings->proxies[SOCKS_PROXY]->host != NULL
				 && settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[SOCKS_PROXY];
		}
		break;

	case GTCP_PROXY_OTHER:
	case GTCP_PROXY_SOCKS4:
	case GTCP_PROXY_SOCKS5:
		if (settings->proxies[SOCKS_PROXY]->host != NULL
			&& settings->mode != GTCP_PROXY_DIRECT)
		{
			proxy = settings->proxies[SOCKS_PROXY];
		}
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	return proxy;
}


/* This function is the meat of the proxy stuff */
GTcpConnectionStatus
_gtcp_proxy_traverse_proxy (GTcpProxy * proxy,
							const gchar * address,
							guint16 port,
							gint sockfd,
							gboolean is_server)
{
	GTcpConnectionStatus status = GTCP_CONNECTION_ERROR_PROXY_ERROR;
	guchar *data = NULL,
	 *recv_buffer = NULL;
	gsize data_size = 0,
	  total_bytes_written = 0;
	gssize retval = 0;
	struct in_addr proxy_in;

	if (proxy == NULL)
		return status;

	/* And so it gets icky. */
	switch (proxy->type)
	{
		/*
		 * Info on HTTP Proxies:
		 * IETF RFCs 2068, 2817
		 */
	case GTCP_PROXY_HTTP:
		if (proxy->use_auth)
		{
			if (proxy->username != NULL && proxy->passwd == NULL)
			{
				data = g_strdup_printf ("CONNECT: %s:%u HTTP/1.1\r\n"
										"Host: %s:%u\r\n"
										"Proxy-Authentication: userid=%s\r\n"
										"\r\n", address, port, address, port,
										proxy->username);
			}
			else if (proxy->username == NULL && proxy->passwd != NULL)
			{
				data = g_strdup_printf ("CONNECT: %s:%u HTTP/1.1\r\n"
										"Host: %s:%u\r\n"
										"Proxy-Authentication: password=%s\r\n"
										"\r\n", address, port, address, port,
										proxy->passwd);
			}
			else
			{
				data = g_strdup_printf ("CONNECT: %s:%u HTTP/1.1\r\n"
										"Host: %s:%u\r\n"
										"Proxy-Authentication: user-pass=%s:%s\r\n"
										"\r\n", address, port, address, port,
										proxy->username, proxy->passwd);
			}
		}
		else
		{
			data = g_strdup_printf ("CONNECT: %s:%u", address, port);
		}
		data_size = strlen (data);

		/* Connection request */
		while (total_bytes_written < data_size)
		{
			retval = send (sockfd, data + total_bytes_written,
						   data_size - total_bytes_written, MSG_NOSIGNAL);

			if (retval < 0)
				return status;

			total_bytes_written += retval;
		}
		g_free (data);
		data = NULL;
		total_bytes_written = 0;
		data_size = 0;

		recv_buffer = g_new0 (gchar, 30);

		retval = recv (sockfd, recv_buffer, 30, MSG_NOSIGNAL);

		/* If the recv failed */
		if (retval < 0)
		{
			g_free (recv_buffer);
			return status;
		}

		/* Re-use data_size, we don't need it anymore. Sick, I know */
		sscanf (recv_buffer, "HTTP/%*f %u %*s", &data_size);

		switch (data_size)
		{
		case 200:
			status = GTCP_CONNECTION_CONNECTED;
			break;
		default:
			status = GTCP_CONNECTION_ERROR_PROXY_ERROR;
			break;
		}

		g_free (recv_buffer);
		break;

	case GTCP_PROXY_FTP:
		break;

	case GTCP_PROXY_SSL:
		break;

		/*
		 * Info on SOCKSv4:
		 * http://www.socks.nec.com/protocol/socks4.protocol
		 * http://www.socks.nec.com/protocol/socks4a.protocol
		 */
	case GTCP_PROXY_SOCKS4:
		data = (guchar *) g_new0 (GTcpSocks4Data, 1);
		data_size = sizeof (GTcpSocks4Data);

		if (inet_aton (address, &proxy_in) == 0)
			return GTCP_CONNECTION_ERROR_PROXY_ERROR;

		((GTcpSocks4Data *) (data))->version = 4;
		((GTcpSocks4Data *) (data))->command = 0x01;
		((GTcpSocks4Data *) (data))->port = g_htons (port);
		((GTcpSocks4Data *) (data))->address = proxy_in.s_addr;

		/* SOCKSv4 Request */
		while (total_bytes_written < data_size)
		{
			retval = send (sockfd, data + total_bytes_written,
						   data_size - total_bytes_written, MSG_NOSIGNAL);

			if (retval < 0)
				return GTCP_CONNECTION_ERROR_PROXY_ERROR;

			total_bytes_written += retval;
		}
		g_free (data);
		data = NULL;
		total_bytes_written = 0;

		/* SOCKSv4 Username */
		data = (gpointer) g_get_user_name ();
		data_size = strlen (data);

		while (total_bytes_written < data_size)
		{
			retval = send (sockfd, data + total_bytes_written,
						   data_size - total_bytes_written, MSG_NOSIGNAL);

			if (retval < 0)
				return GTCP_CONNECTION_ERROR_PROXY_ERROR;

			total_bytes_written += retval;
		}

		/* Reply */
		recv_buffer = (guchar *) g_new0 (GTcpSocks4Data, 1);
		g_assert (recv_buffer != NULL);

		retval = recv (sockfd, recv_buffer, sizeof (GTcpSocks4Data), MSG_NOSIGNAL);

		/* If the recv failed */
		if (retval < 0)
		{
			g_free (recv_buffer);
			return GTCP_CONNECTION_ERROR_PROXY_ERROR;
		}

		/*
		 * Figure out what happened with the connection (the command field is
		 * reused for reply status codes when it's incoming)
		 */
		switch (((GTcpSocks4Data *) (recv_buffer))->command)
		{
			/* Success */
		case 90:
			status = GTCP_CONNECTION_CONNECTED;
			break;

			/*
			 * This is just the generic "couldn't connect" error. SOCKSv4 doesn't
			 * provide detailed errors
			 */
		case 91:
			status = GTCP_CONNECTION_ERROR_CONNECTION_REFUSED;
			break;

			/* Anything else is the proxy server crapping itself */
		default:
			status = GTCP_CONNECTION_ERROR_PROXY_ERROR;
			break;
		}

		g_free (recv_buffer);
		break;

		/*
		 * Info on SOCKSv5:
		 * IETF RFCs 1928, 1929, 1961
		 */
	case GTCP_PROXY_SOCKS5:
		data = (guchar *) g_new0 (GTcpSocks5NegotiationData, 1);
		data_size = sizeof (GTcpSocks5NegotiationData);

		((GTcpSocks5NegotiationData *) (data))->version = 5;
		((GTcpSocks5NegotiationData *) (data))->num_methods = 1;
		((GTcpSocks5NegotiationData *) (data))->methods = 0;

		/* SOCKSv5 negotiation data */
		while (total_bytes_written < data_size)
		{
			retval = send (sockfd, data + total_bytes_written,
						   data_size - total_bytes_written, MSG_NOSIGNAL);

			if (retval < 0)
			{
				g_free (data);
				return GTCP_CONNECTION_ERROR_PROXY_ERROR;
			}

			total_bytes_written += retval;
		}
		/* Don't need data1 anymore */
		g_free (data);
		total_bytes_written = 0;

		recv_buffer = g_new0 (guint8, 2);
		g_assert (recv_buffer != NULL);

		retval = recv (sockfd, recv_buffer, 2, MSG_NOSIGNAL);

		/* If the recv failed */
		if (retval < 0)
		{
			g_free (recv_buffer);
			return GTCP_CONNECTION_ERROR_PROXY_ERROR;
		}
		/* ...or the method field of the recieved data is 0xFF, indicating the server
		   is telling us "can't do that" */
		if (*((guint8 *) (recv_buffer + 1)) == 0xFF)
		{
			g_free (recv_buffer);
			return GTCP_CONNECTION_ERROR_PROXY_ERROR;
		}

		g_free (recv_buffer);

		if (inet_aton (address, &proxy_in) == 0)
			return GTCP_CONNECTION_ERROR_PROXY_ERROR;

		data = (guchar *) g_new0 (GTcpSocks5RequestData, 1);
		data_size = sizeof (GTcpSocks5RequestData);

		((GTcpSocks5RequestData *) (data))->version = 5;
		((GTcpSocks5RequestData *) (data))->command = SOCKS5_COMMAND_CONNECT;
		((GTcpSocks5RequestData *) (data))->reserved = 0;
		((GTcpSocks5RequestData *) (data))->address_type = SOCKS5_TYPE_IPV4;
		((GTcpSocks5RequestData *) (data))->address = proxy_in.s_addr;
		((GTcpSocks5RequestData *) (data))->port = g_htons (port);

		/* Send the SOCKS5 request data */
		while (total_bytes_written < data_size)
		{
			retval = send (sockfd, data + total_bytes_written,
						   data_size - total_bytes_written, MSG_NOSIGNAL);

			if (retval < 0)
			{
				g_free (data);
				return GTCP_CONNECTION_ERROR_PROXY_ERROR;
			}

			total_bytes_written += retval;
		}
		/* Don't need data1 anymore */
		g_free (data);
		total_bytes_written = 0;

		recv_buffer = (guchar *) g_new0 (GTcpSocks5RequestData, 1);

		retval = recv (sockfd, recv_buffer, sizeof (GTcpSocks5RequestData), MSG_NOSIGNAL);

		/* If the recv failed */
		if (retval < 0)
		{
			g_free (recv_buffer);
			return GTCP_CONNECTION_ERROR_PROXY_ERROR;
		}

		/*
		 * Figure out what happened with the connection (the command field is
		 * reused for reply status codes when it's incoming)
		 */
		switch (((GTcpSocks5RequestData *) (recv_buffer))->command)
		{
		case 0x00:
			status = GTCP_CONNECTION_CONNECTED;
			break;
			/* NETUNREACH */
		case 0x03:
			/* host unreach */
		case 0x04:
			status = GTCP_CONNECTION_ERROR_NETWORK_UNREACHABLE;
			break;
			/* Connection refused */
		case 0x05:
			status = GTCP_CONNECTION_ERROR_CONNECTION_REFUSED;
			break;
			/* TTL expired */
		case 0x06:
			status = GTCP_CONNECTION_ERROR_TIMEOUT;
			/*
			 * Anything else:
			 * 0x01 == problem inside server
			 * 0x02 == not allowed by server ruleset
			 * 0x07 == socks command not supported
			 * 0x08 == address type not supported
			 * 0x09 -> 0xFF == unassigned
			 */
		default:
			status = GTCP_CONNECTION_ERROR_PROXY_ERROR;
			break;
		}

		g_free (recv_buffer);
		break;

	default:
		g_assert_not_reached ();
		break;
	}

	return status;
}


/* ********************************** *
 *  PUBLIC API                        *
 * ********************************** */


/**
 * gtcp_proxy_get_use_proxy:
 * @type: the #GTcpProxyType for this connection.
 * @address: the remote address to check the proxy for.
 *
 * This function checks #GTcpProxyType & the address, and returns TRUE if a proxy
 * will be used, or FALSE, if one won't. This is useful if you need to check to
 * see if you can create a #GTcpServer without problems.
 *
 * Returns: a #gboolean if a proxy will be used for this connection.
 *
 * Since: 1.0
 **/
gboolean
gtcp_proxy_get_use_proxy (GTcpProxyType type,
						  const gchar * address)
{
	GSList *exceptions;
	gboolean retval = FALSE;

	g_return_val_if_fail (address != NULL, FALSE);
	g_return_val_if_fail (address[0] != '\0', FALSE);

	_gtcp_proxy_initialize ();

	for (exceptions = settings->exceptions; exceptions != NULL;
		 exceptions = exceptions->next)
	{
		if (g_ascii_strcasecmp (address, exceptions->data) == 0)
		{
			return FALSE;
		}
	}

	switch (type)
	{
	case GTCP_PROXY_HTTP:
		retval = (settings->proxies[HTTP_PROXY]->use_proxy
				  && settings->mode != GTCP_PROXY_DIRECT);
		break;
	case GTCP_PROXY_FTP:
		retval = (settings->proxies[FTP_PROXY]->use_proxy
				  && settings->mode != GTCP_PROXY_DIRECT);
		break;
	case GTCP_PROXY_SSL:
		retval = (settings->proxies[SSL_PROXY]->use_proxy
				  && settings->mode != GTCP_PROXY_DIRECT);
		break;
	case GTCP_PROXY_OTHER:
	case GTCP_PROXY_SOCKS4:
	case GTCP_PROXY_SOCKS5:
		retval = (settings->proxies[SOCKS_PROXY]->use_proxy
				  && settings->mode != GTCP_PROXY_DIRECT);
		break;
	default:
		g_assert_not_reached ();
		break;
	}

	_gtcp_proxy_shutdown ();

	return retval;
}
