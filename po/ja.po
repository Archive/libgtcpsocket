# libgctpsocket ja.po
# Copyright (C) 2003 Free Software Foundation, Inc.
# This file is distributed under the same license as the libgctpsocket package.
# Takeshi AIHANA <aihana@gnome.gr.jp>, 2003.
#
msgid ""
msgstr ""
"Project-Id-Version: libgtcpsocket HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-06-26 23:19+0900\n"
"PO-Revision-Date: 2003-06-26 23:18+0900\n"
"Last-Translator: Takeshi AIHANA <aihana@gnome.gr.jp>\n"
"Language-Team: Japanese <gnome-translation@gnome.gr.jp>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/libgtcpsocket.schemas.in.h:1
msgid "Authenticate HTTP Proxy Connections"
msgstr "HTTP プロキシ接続の認証"

#: data/libgtcpsocket.schemas.in.h:2
msgid "Enables proxy settings when accessing HTTP over the internet."
msgstr "インターネット越しの HTTP アクセス時にプロキシの設定を有効にします。"

#: data/libgtcpsocket.schemas.in.h:3
msgid "FTP Proxy Port"
msgstr "FTP プロキシのポート"

#: data/libgtcpsocket.schemas.in.h:4
msgid "HTTP Proxy Location"
msgstr "HTTP プロキシの場所"

#: data/libgtcpsocket.schemas.in.h:5
msgid "HTTP Proxy Port"
msgstr "HTTP プロキシのポート"

#: data/libgtcpsocket.schemas.in.h:6
msgid "HTTP Proxy User Name"
msgstr "HTTP プロキシのユーザ名"

#: data/libgtcpsocket.schemas.in.h:7
msgid ""
"If true, then connections to the proxy server require authentication. The "
"username/password defined by /system/http_proxy/authentication_user and /"
"system/http_proxy/authentication_password."
msgstr ""
"TRUE にすると、プロキシ・サーバへの接続に認証が必要になります。ユーザ名とパス"
"ワードはそれぞれ /system/http_proxy/authentication_user と /system/"
"http_proxy/authentication_password で定義されます。"

#: data/libgtcpsocket.schemas.in.h:8
msgid "Proxy host name"
msgstr "プロキシのホスト名"

#: data/libgtcpsocket.schemas.in.h:9
msgid "Proxy port"
msgstr "プロキシのポート番号"

#: data/libgtcpsocket.schemas.in.h:10
msgid "SOCKS Proxy Host Name"
msgstr "SOCKS プロキシのホスト名"

#: data/libgtcpsocket.schemas.in.h:11
msgid "SOCKS is Version 5"
msgstr "SOCKS はバージョン 5"

#: data/libgtcpsocket.schemas.in.h:12
msgid "SSL Proxy Host Name"
msgstr "SSL プロキシのホスト名"

#: data/libgtcpsocket.schemas.in.h:13
msgid "SSL Proxy Port"
msgstr "SSL プロキシのポート番号"

#: data/libgtcpsocket.schemas.in.h:14
msgid "The location of the FTP proxy server."
msgstr "FTP プロキシ・サーバのロケーションです。"

#: data/libgtcpsocket.schemas.in.h:15
msgid "The location of the proxy server to use for HTTP connections."
msgstr "HTTP 接続に使用するプロキシ・サーバのロケーションです。"

#: data/libgtcpsocket.schemas.in.h:16
msgid "The location of the proxy server which handles encrypted connections."
msgstr "暗号化した接続を扱うプロキシ・サーバのロケーションです。"

#: data/libgtcpsocket.schemas.in.h:17
msgid ""
"The location of the proxy server which handles non-HTTP and non-SSL "
"connections."
msgstr "非 HTTP と 非 SSL 接続を扱うプロキシ・サーバのロケーションです。"

#: data/libgtcpsocket.schemas.in.h:18
msgid "The password to use when authenticating with the HTTP proxy server."
msgstr "HTTP プロキシ・サーバの認証時に使用するパスワードです。"

#: data/libgtcpsocket.schemas.in.h:19
msgid "The port on the proxy server defined by /system/http_proxy/host."
msgstr "/system/http_proxy/host で定義したプロキシ・サーバのポート番号です。"

#: data/libgtcpsocket.schemas.in.h:20
msgid "The port on the proxy server defined by /system/proxy/ftp_host."
msgstr "/system/proxy/ftp_host で定義したプロキシ・サーバのポート番号です。"

#: data/libgtcpsocket.schemas.in.h:21
msgid "The port on the proxy server defined by /system/proxy/socks_host."
msgstr "/system/proxy/socks_host で定義したプロキシ・サーバのポート番号です。"

#: data/libgtcpsocket.schemas.in.h:22
msgid "The port on the proxy server defined by /system/proxy/ssl_host."
msgstr "/system/proxy/ssl_host で定義されたプロキシ・サーバのポート番号です。"

#: data/libgtcpsocket.schemas.in.h:23
msgid "The username to use when authenticating with the HTTP proxy server."
msgstr "HTTP プロキシ・サーバの認証時に使用するユーザ名です。"

#: data/libgtcpsocket.schemas.in.h:24
msgid "Use a proxy when accessing HTTP"
msgstr "HTTP アクセス時にプロキシを使用する"

#: data/libgtcpsocket.schemas.in.h:25
msgid ""
"Whether or not the machine defined by /system/proxy/socks_host is a SOCKS5 "
"proxy."
msgstr ""
"/system/proxy/socks_host で定義されたマシンが SOCKS5 プロキシかどうかを示しま"
"す。"

#: data/libgtcpsocket.schemas.in.h:26
msgid "http proxy password"
msgstr "HTTP プロキシのパスワード"

#: libgtcpsocket/gtcp-connection.c:1434
msgid "Address"
msgstr "アドレス"

#: libgtcpsocket/gtcp-connection.c:1435
msgid "The hostname or IP address to connect to."
msgstr "接続先のホスト名または IP-アドレスです。"

#: libgtcpsocket/gtcp-connection.c:1439
msgid "Port Number"
msgstr "ポート番号"

#: libgtcpsocket/gtcp-connection.c:1440
msgid "The port number to connect to."
msgstr "接続先のポート番号です。"

#: libgtcpsocket/gtcp-connection.c:1445 libgtcpsocket/gtcp-server.c:419
msgid "Local Address"
msgstr "ローカル・アドレス"

#: libgtcpsocket/gtcp-connection.c:1446
msgid "The hostname or IP address of this computer."
msgstr "ご利用のコンピュータのホスト名または IP-アドレスです。"

#: libgtcpsocket/gtcp-connection.c:1450
msgid "Local Port Number"
msgstr "ローカルのポート番号"

#: libgtcpsocket/gtcp-connection.c:1451
msgid "The local port number to connect through."
msgstr "経由点として接続するローカルのポート番号です。"

#: libgtcpsocket/gtcp-connection.c:1457
msgid "Connection Style"
msgstr "接続形式"

#: libgtcpsocket/gtcp-connection.c:1458
msgid ""
"What style of connection this will be, used to determine proxy information."
msgstr "プロキシ情報を決定するために使用する、この接続のスタイルです。"

#: libgtcpsocket/gtcp-connection.c:1465
msgid "Use SSL"
msgstr "SSL を使用する"

#: libgtcpsocket/gtcp-connection.c:1466
msgid "Whether or not to use SSL in this connection."
msgstr "この接続で SSL を使用するかどうかです"

#: libgtcpsocket/gtcp-connection.c:1471 libgtcpsocket/gtcp-server.c:466
msgid "Buffer Size"
msgstr "バッファ・サイズ"

#: libgtcpsocket/gtcp-connection.c:1472 libgtcpsocket/gtcp-server.c:467
msgid "The max incoming buffer size."
msgstr "incoming のバッファ・サイズ (最大値) です"

#: libgtcpsocket/gtcp-connection.c:1479
msgid "IP Address"
msgstr "IP-アドレス"

#: libgtcpsocket/gtcp-connection.c:1480
msgid "The IP address connected to."
msgstr "接続先の IP-アドレスです"

#: libgtcpsocket/gtcp-connection.c:1484
msgid "Connection Status"
msgstr "接続ステータス"

#: libgtcpsocket/gtcp-connection.c:1485
msgid "The current status of the connection."
msgstr "現在の接続状態です"

#: libgtcpsocket/gtcp-connection.c:1491
msgid "Connection is a Server"
msgstr "サーバ接続"

#: libgtcpsocket/gtcp-connection.c:1492
msgid ""
"Whether the connection was created from a server or not. For internal use by "
"GTcpServer only."
msgstr ""
"接続がサーバから生成されているかどうかを示し、GTcpServer の内部でのみ使用しま"
"す"

#: libgtcpsocket/gtcp-connection.c:1499 libgtcpsocket/gtcp-server.c:474
msgid "Bytes Read"
msgstr "入力バイト数"

#: libgtcpsocket/gtcp-connection.c:1500 libgtcpsocket/gtcp-server.c:475
msgid "The total bytes read."
msgstr "入力バイト数の合計です"

#: libgtcpsocket/gtcp-connection.c:1504 libgtcpsocket/gtcp-server.c:479
msgid "Bytes Written"
msgstr "出力バイト数"

#: libgtcpsocket/gtcp-connection.c:1505 libgtcpsocket/gtcp-server.c:480
msgid "The total bytes written."
msgstr "出力バイト数の合計です"

#: libgtcpsocket/gtcp-connection.c:1509
msgid "Socket File Descriptor"
msgstr "ソケット・ファイルのディスクリプタ"

#: libgtcpsocket/gtcp-connection.c:1510
msgid "The socket file descriptor. For internal use by GTcpServer only."
msgstr "ソケット・ファイルのディスクリプタで、GtpServer の内部でのみ使用します"

#: libgtcpsocket/gtcp-errors.c:45
#, c-format
msgid "Search for \"%s\" in progress..."
msgstr "\"%s\" の検索中..."

#: libgtcpsocket/gtcp-errors.c:47
#, c-format
msgid "%s found"
msgstr "%s が見つかりました"

#: libgtcpsocket/gtcp-errors.c:49
#, c-format
msgid ""
"\"%s\" could not be found. The name might be misspelled, or it may not exist."
msgstr ""
"\"%s\" が見つかりませんでした。名前のスペルが間違っているか、または存在しない"
"可能性があります。"

#: libgtcpsocket/gtcp-errors.c:52
#, c-format
msgid ""
"The DNS lookup server could not be contacted to find %s. The network may be "
"down, or the DNS server may be broken."
msgstr ""
"%s を検索に使用する DNS の検索サーバに接続できませんでした。ネットワークがダ"
"ウンしているか、DNS サーバが壊れているかもしれません。"

#: libgtcpsocket/gtcp-errors.c:55
msgid ""
"The DNS lookup server is temporarily unavailable, try connecting in a few "
"minutes."
msgstr "DNS の検索サーバが一時的に利用できません。あとで再試行してみて下さい。"

#: libgtcpsocket/gtcp-errors.c:58
#, c-format
msgid ""
"The network is misconfigured, and \"%s\" cannot be connected to. Contact "
"your network administrator or ISP for more information."
msgstr ""
"ネットワークの設定が間違っているため、\"%s\" に接続できません。詳細はネット"
"ワーク管理者または ISP に問い合わせて下さい。"

#: libgtcpsocket/gtcp-errors.c:61
msgid ""
"Your computer is unable to use a real application, install a threads "
"implementation or upgrade. :-P"
msgstr ""
"お使いのコンピュータが実際のアプリケーションを使用できません。スレッドの実装"
"をインストールするか、アップグレードして下さい :-P"

#: libgtcpsocket/gtcp-errors.c:69
#, c-format
msgid ""
"%s has will not allow you to connect to the requested service. The port "
"number may be incorrect."
msgstr ""
"%s の指定したサービスに接続できません。ポート番号が間違っている可能性がありま"
"す。"

#: libgtcpsocket/gtcp-errors.c:72
#, c-format
msgid ""
"The connection attempt took too long to complete. %s may be down, or you may "
"have been disconnected from the network."
msgstr ""
"接続の試みが完了するまで時間がかかりすぎです。%s がダウンしているか、または"
"ネットワークへの接続が切れている可能性があります。"

#: libgtcpsocket/gtcp-errors.c:75
#, c-format
msgid ""
"You cannot connect to %s, because it is on a network you cannot connect to. "
"The address may be incorrect, or you may have been disconnected."
msgstr ""
"接続できないネットワーク上にいるため、%s に接続できません。アドレスが間違って"
"いるか、または接続が切れている可能性があります。"

#: libgtcpsocket/gtcp-errors.c:79
#, c-format
msgid ""
"You cannot connect to %s, because your computer or firewall is configured to "
"prevent it."
msgstr ""
"お使いのコンピュータあるいはファイヤーウォールの設定により %s に接続できませ"
"ん。"

#: libgtcpsocket/gtcp-errors.c:82
#, c-format
msgid ""
"You cannot connect to %s, because your computer does not support threaded "
"applications."
msgstr ""
"お使いのコンピュータではスレッド対応アプリケーションをサポートしていないため "
"%s に接続できません。"

#: libgtcpsocket/gtcp-errors.c:85
#, c-format
msgid ""
"You cannot connect to %s, because your proxy server could not be found. Your "
"computer may be misconfigured, or the proxy server may be down."
msgstr ""
"お使いのプロキシ・サーバが見つからないため %s に接続できません。お使いのコン"
"ピュータの設定が間違っているか、またはプロキシ・サーバがダウンしている可能性"
"があります。"

#: libgtcpsocket/gtcp-errors.c:94
#, c-format
msgid "Your computer is now accepting connections to port %u."
msgstr "お使いのコンピュータがポート %u への接続を受け入れています。"

#: libgtcpsocket/gtcp-errors.c:96
#, c-format
msgid "Your computer is preparing to accept connections to port %u."
msgstr "お使いのコンピュータがポート %u への接続の受け入れ準備中です。"

#: libgtcpsocket/gtcp-errors.c:98
#, c-format
msgid ""
"You cannot accept connections on port %u, because your computer does not "
"have a local address."
msgstr ""
"お使いのコンピュータにローカル・アドレスが割り当てられていないため、ポート %"
"u への接続を許可できません。"

#: libgtcpsocket/gtcp-errors.c:101
#, c-format
msgid ""
"You cannot accept connections on port %u, because you do not have permission "
"to use TCP/IP connections."
msgstr ""
"TCP/IP 接続を利用するための権限がないため、ポート %u への接続を許可できませ"
"ん。"

#: libgtcpsocket/gtcp-errors.c:104
#, c-format
msgid ""
"You cannot accept connections on port %u, because your computer is out of "
"memory for networking purposes."
msgstr ""
"お使いのコンピュータのネットワーク用メモリが足りないため、ポート %u への接続"
"を許可できません。"

#: libgtcpsocket/gtcp-errors.c:107
#, c-format
msgid ""
"You cannot accept connections on port %u, because your computer has too many "
"running applications."
msgstr ""
"お使いのコンピュータ上で非常に多くのアプリケーションが動作しているため、ポー"
"ト %u への接続を許可できません。"

#: libgtcpsocket/gtcp-errors.c:110
#, c-format
msgid ""
"You cannot accept connections on port %u, because that port is already being "
"used."
msgstr "ポート %u は既に使用中のため、そのポートの接続を許可できません。 "

#: libgtcpsocket/gtcp-errors.c:113
#, c-format
msgid ""
"You cannot accept connections on port %u, because you do not have permission "
"to accept connections on ports lower than 1024."
msgstr ""
"1024 未満のポートに接続するための権限がないため、ポート %u への接続を許可でき"
"ません。"

#: libgtcpsocket/gtcp-errors.c:116
#, c-format
msgid ""
"You cannot accept connections on port %u, because an error occurred inside "
"the networking library."
msgstr ""
"ネットワーク・ライブラリ内部でエラーが発生したため、ポート %u への接続を許可"
"できません。"

#: libgtcpsocket/gtcp-errors.c:147 libgtcpsocket/gtcp-errors.c:178
#: libgtcpsocket/gtcp-errors.c:207
msgid "This application is doing some strange foo."
msgstr "このアプリケーションの動作がおかしいです。"

#: libgtcpsocket/gtcp-server.c:420
msgid "The hostname or IP address of this computer -- optional."
msgstr "このコンピュータのホスト名または IP-アドレス -- オプション"

#: libgtcpsocket/gtcp-server.c:426
msgid "The port number to allow connections to."
msgstr "接続可能なポート番号"

#: libgtcpsocket/gtcp-server.c:432
msgid "Whether or not to use SSL connections to this server."
msgstr "このサーバへの SSL 接続を利用するかどうか"

#: libgtcpsocket/gtcp-server.c:439
msgid "Maximum Simultaneous Connections"
msgstr "同時接続数 (最大値)"

#: libgtcpsocket/gtcp-server.c:440
msgid "The maximum number of connections to allow."
msgstr "接続できる最大数"

#: libgtcpsocket/gtcp-server.c:445
msgid "Open Connections"
msgstr "オープンする接続数"

#: libgtcpsocket/gtcp-server.c:446
msgid "A GList of open connections."
msgstr "オープンする接続の GList"

#: libgtcpsocket/gtcp-server.c:450
msgid "Kill Open Connections Style"
msgstr "オープンする接続スタイルの強制クローズ"

#: libgtcpsocket/gtcp-server.c:451
msgid "Whether to kill open GTcpConnections or not and when."
msgstr ""
"オープンする GTcpConnections を強制的にクローズするかどうか、いつクローズする"
"か"

#: libgtcpsocket/gtcp-server.c:459
msgid "Performs Reverse Lookups"
msgstr "リバース検索を実行する"

#: libgtcpsocket/gtcp-server.c:460
msgid "Whether or not to perform reverse lookups on incoming connections."
msgstr "incoming 接続でリバース検索を実施するかどうか"
