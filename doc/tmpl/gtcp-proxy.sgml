<!-- ##### SECTION Title ##### -->
Proxy Support

<!-- ##### SECTION Short_Description ##### -->
Functions for determining proxy support.

<!-- ##### SECTION Long_Description ##### -->
<para>
The GTcpSocket library automatically forwards outgoing connections through
configured proxy servers. The configuration is stored in GConf, and GTcpSocket
reads the same configuration as GnomeVFS, and by extension, the GNOME Desktop.
</para>

<para>
The GConf keys used to store proxy settings are:
<informaltable>
<tgroup cols="3" align="left" colsep="1" rowsep="1">
	<thead><row>
		<entry>Key</entry>
		<entry>Type</entry>
		<entry>Description</entry>
	</row></thead>
	<tbody>
		<row>
			<entry>/system/http_proxy/use_http_proxy</entry>
			<entry>Boolean</entry>
			<entry>Use a proxy server to connect to HTTP servers.</entry>
		</row>
		<row>
			<entry>/system/http_proxy/host</entry>
			<entry>String</entry>
			<entry>The hostname of the HTTP proxy server.</entry>
		</row>
		<row>
			<entry>/system/http_proxy/port</entry>
			<entry>Integer</entry>
			<entry>The port of the HTTP proxy server defined in /system/http_proxy/host.</entry>
		</row>
		<row>
			<entry>/system/http_proxy/use_authentication</entry>
			<entry>Boolean</entry>
			<entry>Authenticate when using the HTTP proxy server.</entry>
		</row>
		<row>
			<entry>/system/http_proxy/authentication_user</entry>
			<entry>String</entry>
			<entry>The username to use when authenticating with the HTTP proxy server defined in /system/http_proxy/host.</entry>
		</row>
		<row>
			<entry>/system/http_proxy/authentication_password</entry>
			<entry>String</entry>
			<entry>The password to use when authenticating with the HTTP proxy server defined in /system/http_proxy/host.</entry>
		</row>
		<row>
			<entry>/system/proxy/ftp_host</entry>
			<entry>String</entry>
			<entry>The hostname of the FTP proxy server.</entry>
		</row>
		<row>
			<entry>/system/proxy/ftp_port</entry>
			<entry>Integer</entry>
			<entry>The port of the FTP proxy server defined in /system/proxy/ftp_host.</entry>
		</row>
		<row>
			<entry>/system/proxy/ssl_host</entry>
			<entry>String</entry>
			<entry>The hostname of the SSL proxy server.</entry>
		</row>
		<row>
			<entry>/system/proxy/ssl_port</entry>
			<entry>Integer</entry>
			<entry>The port of the SSL proxy server defined in /system/proxy/ssl_host.</entry>
		</row>
		<row>
			<entry>/system/proxy/socks_host</entry>
			<entry>String</entry>
			<entry>The hostname of the SOCKS proxy server.</entry>
		</row>
		<row>
			<entry>/system/proxy/socks_port</entry>
			<entry>Integer</entry>
			<entry>The port of the SOCKS proxy server defined in /system/proxy/socks_host.</entry>
		</row>
		<row>
			<entry>/system/proxy/socks_is_v5</entry>
			<entry>Boolean</entry>
			<entry>The SOCKS proxy server defined in /system/proxy/socks_host uses SOCKS version 5.</entry>
		</row>
	</tbody>
</tgroup>
</informaltable>
</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### ENUM GTcpProxyType ##### -->
<para>

</para>

@GTCP_PROXY_HTTP: 
@GTCP_PROXY_FTP: 
@GTCP_PROXY_OTHER: 
@GTCP_PROXY_SSL: 
@GTCP_PROXY_SOCKS4: 
@GTCP_PROXY_SOCKS5: 

<!-- ##### FUNCTION gtcp_proxy_get_use_proxy ##### -->
<para>

</para>

@type: 
@address: 
@Returns: 


